package me.mathiaseklund.vasbysurvival.util;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryUtil {

	public static void addFiller(Inventory inv) {
		ItemStack is = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[reportgui.filler]"));
		is.setItemMeta(im);

		for (int i = 0; i < inv.getSize(); i++) {
			if (inv.getItem(i) == null) {
				inv.setItem(i, is);
			}
		}
	}
}
