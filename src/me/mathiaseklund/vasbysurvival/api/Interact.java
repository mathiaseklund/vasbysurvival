package me.mathiaseklund.vasbysurvival.api;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.InventoryUtil;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Interact {

	public static void openInteractMenu(Player p, Player t) {
		User user = Users.getUser(p.getUniqueId().toString());
		User target = Users.getUser(t.getUniqueId().toString());
		Inventory inv = Bukkit.createInventory(null, 9,
				ChatColor.translateAlternateColorCodes('&', "Player Interaction"));
		ItemStack is = null;
		ItemMeta im = null;

		// Friend option
		if (Friends.isFriends(user.getId(), target.getId())) {
			is = new ItemStack(Material.BOOK);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[interact.removefriend] " + target.getId() + " ") + ChatColor.RED
					+ "Remove Friend");
			is.setItemMeta(im);
		} else {
			is = new ItemStack(Material.BOOK);
			im = is.getItemMeta();
			im.setDisplayName(
					Util.HideString("[interact.addfriend] " + target.getId() + " ") + ChatColor.GREEN + "Add Friend");
			is.setItemMeta(im);
		}
		inv.addItem(is);

		// Trade option
		is = new ItemStack(Material.CHEST_MINECART);
		im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[interact.trade] " + target.getId() + " ") + ChatColor.YELLOW + "Trade");
		is.setItemMeta(im);
		inv.addItem(is);

		InventoryUtil.addFiller(inv);

		p.openInventory(inv);
	}
}
