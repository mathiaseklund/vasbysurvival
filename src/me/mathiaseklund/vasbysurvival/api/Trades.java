package me.mathiaseklund.vasbysurvival.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Trades {

	static HashMap<String, Inventory> trade_sender = new HashMap<String, Inventory>();
	static HashMap<String, Inventory> trade_target = new HashMap<String, Inventory>();
	static HashMap<String, String> trade_with = new HashMap<String, String>();
	static HashMap<String, Boolean> trade_accepted = new HashMap<String, Boolean>();

	public static void requestTrade(String uuid, String targetuuid) {
		Util.debug("Requesting trade.");

		User sender = Users.getUser(uuid);
		if (sender.inStaffMode()) {
			Util.message(sender.getPlayer(), "&4ERROR:&7 Can't send trade offer while in staff mode.");
		} else {
			User target = Users.getUser(targetuuid);
			if (sender.getIncomingTradeRequests().contains(targetuuid)) {
				// TODO accept request
			} else {
				if (target.getIncomingTradeRequests().contains(uuid)) {
					Util.message(sender.getPlayer(),
							"&4ERROR:&7 You've already sent a trade request to target player.");
				} else {
					sender.addOutgoingTradeRequest(targetuuid);
					target.addIncomingTradeRequest(uuid);

					// SEND TRADE RECEIVED MESSAGE

					TextComponent t = new TextComponent();

					TextComponent s = new TextComponent(sender.getName() + " ");
					s.setColor(ChatColor.RED);

					TextComponent m = new TextComponent("has sent you a trade request. ");
					m.setColor(ChatColor.YELLOW);

					TextComponent a = new TextComponent("Click here to Accept");
					a.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/trade accept " + sender.getId()));
					a.setColor(ChatColor.GREEN);

					TextComponent o = new TextComponent(" OR ");
					o.setColor(ChatColor.YELLOW);

					TextComponent d = new TextComponent("Click here to Decline");
					d.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/trade decline " + sender.getId()));
					d.setColor(ChatColor.RED);

					t.addExtra(s);
					t.addExtra(m);

					a.addExtra(o);
					a.addExtra(d);

					target.getPlayer().spigot().sendMessage(t);
					target.getPlayer().spigot().sendMessage(a);
					// SEND TRADE SENT MESSAGE
					Util.message(sender.getPlayer(), "&eYou've sent a trade request to &c" + target.getName());
				}
			}
		}
	}

	public static void acceptTrade(String t, String s) {
		User sender = Users.getUser(s);
		User target = Users.getUser(t);
		if (target.inStaffMode()) {
			Util.message(target.getPlayer(), "&4ERROR:&7 Can't accept trade whilst in staff mode.");
		} else {
			if (sender.getOutgoingTradeRequests().contains(t)) {
				sender.removeOutgoingTradeRequest(t);
				target.removeIncomingTradeRequest(s);
				trade_with.put(sender.getId(), target.getId());
				trade_with.put(target.getId(), sender.getId());
				openTrade(sender, target);
			} else {
				Util.message(target.getPlayer(), "&4ERROR:&7 Trade not found.");
			}
		}
	}

	public static void declineTrade(String t, String s) {
		User sender = Users.getUser(s);
		User target = Users.getUser(t);
		if (sender.getOutgoingTradeRequests().contains(t)) {
			sender.removeOutgoingTradeRequest(t);
			target.removeIncomingTradeRequest(s);
			Util.message(sender.getPlayer(), "&c" + target.getName() + "&e Declined your trade request.");
			Util.message(target.getPlayer(), "&eYou've Declined &c" + sender.getName() + "&e's trade request.");
		} else {
			Util.message(target.getPlayer(), "&4ERROR:&7 Trade not found.");
		}
	}

	public static void openTrade(User sender, User target) {
		Inventory inv = Bukkit.createInventory(null, 54,
				Util.HideString("[tui]") + sender.getName() + " || " + target.getName());
		trade_sender.put(sender.getId(), inv);
		trade_target.put(target.getId(), inv);

		ItemStack is = new ItemStack(Material.RED_STAINED_GLASS_PANE);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[tui]") + ChatColor.GREEN + "Accept Trade");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "Trade is currently not accepted.");
		lore.add(ChatColor.YELLOW + "Click to accept");
		is.setItemMeta(im);
		inv.setItem(2, is);
		inv.setItem(6, is);

		is = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
		im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[tui]"));
		is.setItemMeta(im);

		inv.setItem(0, is);
		inv.setItem(1, is);
		inv.setItem(3, is);
		inv.setItem(4, is);
		inv.setItem(5, is);
		inv.setItem(7, is);
		inv.setItem(8, is);
		inv.setItem(9, is);
		inv.setItem(10, is);
		inv.setItem(11, is);
		inv.setItem(12, is);
		inv.setItem(13, is);
		inv.setItem(14, is);
		inv.setItem(15, is);
		inv.setItem(16, is);
		inv.setItem(17, is);
		inv.setItem(22, is);
		inv.setItem(31, is);
		inv.setItem(40, is);
		inv.setItem(49, is);

		sender.getPlayer().openInventory(inv);
		target.getPlayer().openInventory(inv);
	}

	public static boolean isSender(String uuid) {
		if (trade_sender.containsKey(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isTarget(String uuid) {
		if (trade_target.containsKey(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	public static String getTradePartner(String uuid) {
		return trade_with.get(uuid);
	}

	public static void cancelTrade(String s, String t, Inventory inv) {
		User sender = Users.getUser(s);
		User target = Users.getUser(t);

		trade_with.remove(s);
		trade_with.remove(t);

		target.getPlayer().closeInventory();
		if (trade_sender.containsKey(sender.getId())) {
			trade_sender.remove(sender.getId());
		} else if (trade_target.containsKey(sender.getId())) {
			trade_target.remove(sender.getId());
		}

		if (trade_sender.containsKey(target.getId())) {
			trade_sender.remove(target.getId());
		} else if (trade_target.containsKey(target.getId())) {
			trade_target.remove(target.getId());
		}

		Util.message(sender.getPlayer(), "&cTrade with " + target.getName() + " cancelled");
		Util.message(target.getPlayer(), "&cTrade with " + sender.getName() + " cancelled");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = inv.getItem(i);
			if (is != null) {
				if (isSenderView(i)) {
					sender.getPlayer().getInventory().addItem(is);
				} else if (isTargetView(i)) {
					target.getPlayer().getInventory().addItem(is);
				}
			}
		}
	}

	public static void acceptTradeOffer(String uuid) {
		Util.debug(uuid + " wants to accept trade offer");
		trade_accepted.put(uuid, true);
		if (trade_accepted.containsKey(trade_with.get(uuid))) {
			if (trade_accepted.get(trade_with.get(uuid))) {
				completeTrade(uuid, trade_with.get(uuid));
			} else {
				Util.debug("Trade partner not accepted");
			}
		} else {
			Util.debug("Trade partner not accepted");
		}
		if (isSender(uuid)) {
			Inventory inv = trade_sender.get(uuid);
			ItemStack is = new ItemStack(inv.getItem(2));
			is.setType(Material.LIME_STAINED_GLASS_PANE);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[tui]") + ChatColor.RED + "Decline Trade");
			is.setItemMeta(im);
			inv.setItem(2, is);
		} else if (isTarget(uuid)) {
			Inventory inv = trade_target.get(uuid);
			ItemStack is = new ItemStack(inv.getItem(6));
			is.setType(Material.LIME_STAINED_GLASS_PANE);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[tui]") + ChatColor.RED + "Decline Trade");
			is.setItemMeta(im);
			inv.setItem(6, is);
		}
	}

	public static boolean isSenderView(int slot) {
		if (slot == 18 || slot == 19 || slot == 20 || slot == 21 || slot == 27 || slot == 28 || slot == 29 || slot == 30
				|| slot == 36 || slot == 37 || slot == 38 || slot == 39 || slot == 45 || slot == 46 || slot == 47
				|| slot == 48) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isTargetView(int slot) {
		if (slot == 23 || slot == 24 || slot == 25 || slot == 26 || slot == 32 || slot == 33 || slot == 34 || slot == 35
				|| slot == 41 || slot == 42 || slot == 43 || slot == 44 || slot == 50 || slot == 51 || slot == 52
				|| slot == 53) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean hasAccepted(String uuid) {
		if (trade_accepted.containsKey(uuid)) {
			return trade_accepted.get(uuid);
		} else {
			return false;
		}
	}

	public static void declineTradeOffer(String uuid) {
		Util.debug(uuid + " wants to accept trade offer");
		trade_accepted.put(uuid, false);
		if (isSender(uuid)) {
			Inventory inv = trade_sender.get(uuid);
			ItemStack is = new ItemStack(inv.getItem(2));
			is.setType(Material.RED_STAINED_GLASS_PANE);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[tui]") + ChatColor.GREEN + "Accept Trade");
			is.setItemMeta(im);
			inv.setItem(2, is);

			for (HumanEntity p : inv.getViewers()) {
				Player pl = (Player) p;
				if (!pl.getUniqueId().toString().equalsIgnoreCase(uuid)) {
					pl.updateInventory();
				}
			}
		} else if (isTarget(uuid)) {
			Inventory inv = trade_target.get(uuid);
			ItemStack is = new ItemStack(inv.getItem(6));
			is.setType(Material.RED_STAINED_GLASS_PANE);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[tui]") + ChatColor.GREEN + "Accept Trade");
			is.setItemMeta(im);
			inv.setItem(6, is);

			for (HumanEntity p : inv.getViewers()) {
				Player pl = (Player) p;
				if (!pl.getUniqueId().toString().equalsIgnoreCase(uuid)) {
					pl.updateInventory();
				}
			}
		}

	}

	public static void completeTrade(String u, String p) {
		Util.debug("Completing trade");
		User user = Users.getUser(u);
		User partner = Users.getUser(p);

		trade_with.remove(u);
		trade_with.remove(p);
		trade_accepted.remove(u);
		trade_accepted.remove(p);

		Inventory inv = user.getPlayer().getOpenInventory().getTopInventory();

		Util.message(user.getPlayer(), "&cTrade with " + partner.getName() + " completed");
		Util.message(partner.getPlayer(), "&cTrade with " + user.getName() + " completed");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = inv.getItem(i);
			if (is != null) {

				if (isSenderView(i)) {
					Util.debug("Item Found: " + is.toString());
					if (isSender(u)) {
						Util.debug("Give item to trade target");
						partner.getPlayer().getInventory().addItem(is);
					} else if (isSender(p)) {
						Util.debug("Give item to trade target");
						user.getPlayer().getInventory().addItem(is);
					}
				} else if (isTargetView(i)) {
					Util.debug("Item Found: " + is.toString());
					if (isTarget(u)) {
						Util.debug("Give item to trade sender");
						partner.getPlayer().getInventory().addItem(is);
					} else if (isTarget(p)) {
						Util.debug("Give item to trade sender");
						user.getPlayer().getInventory().addItem(is);
					}
				}
			}
		}
		if (trade_sender.containsKey(user.getId())) {
			trade_sender.remove(user.getId());
		} else if (trade_target.containsKey(user.getId())) {
			trade_target.remove(user.getId());
		}

		if (trade_sender.containsKey(partner.getId())) {
			trade_sender.remove(partner.getId());
		} else if (trade_target.containsKey(partner.getId())) {
			trade_target.remove(partner.getId());
		}
		partner.getPlayer().closeInventory();
		user.getPlayer().closeInventory();
	}

	public static void forceDeclineOffer(String uuid) {
		String partner = trade_with.get(uuid);
		if (hasAccepted(uuid)) {
			declineTradeOffer(uuid);
		}
		if (hasAccepted(partner)) {
			declineTradeOffer(partner);
		}
	}
}
