package me.mathiaseklund.vasbysurvival.api;

import java.util.HashMap;

import me.mathiaseklund.vasbysurvival.objects.User;

public class Users {

	public static HashMap<String, User> users = new HashMap<String, User>();

	/**
	 * Get user object
	 * 
	 * @param uuid
	 * @return
	 */
	public static User getUser(String uuid) {
		if (users.containsKey(uuid)) {
			return users.get(uuid);
		} else {
			User user = new User(uuid);
			return user;
		}
	}

	public static void addUser(String uuid, User user) {
		users.put(uuid, user);
	}

	public static void removeUser(String uuid) {
		users.remove(uuid);
	}
}
