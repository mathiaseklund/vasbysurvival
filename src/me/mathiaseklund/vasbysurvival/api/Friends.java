package me.mathiaseklund.vasbysurvival.api;

import java.util.List;

import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Friends {

	public static List<String> getFriends(String uuid) {
		User user = Users.getUser(uuid);
		return user.getFriends();
	}

	public static boolean isFriends(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		return user.isFriends(targetuuid);
	}

	public static void addFriend(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		if (!isFriends(uuid, targetuuid)) {
			if (getIncomingRequests(uuid).contains(targetuuid)) {
				acceptRequest(uuid, targetuuid);
			} else {
				if (getOutgoingRequests(uuid).contains(targetuuid)) {
					Util.message(user.getPlayer(), "&4ERROR:&7 Friend request already sent to target player.");
				} else {
					addIncomingRequest(targetuuid, uuid);
					addOutgoingRequest(uuid, targetuuid);
				}
			}
		}
	}

	public static void addIncomingRequest(String targetuuid, String uuid) {
		User target = Users.getUser(targetuuid);
		User user = Users.getUser(uuid);
		target.addIncomingRequest(uuid);
		Util.message(target.getPlayer(),
				"&c" + user.getName() + "&e Has sent you a friend request! To accept: &7/f accept " + user.getName());
	}

	public static void addOutgoingRequest(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		User target = Users.getUser(targetuuid);
		user.addOutgoingRequest(targetuuid);
		Util.message(user.getPlayer(), "&eYou've sent a &cfriend request&e to &c" + target.getName());
	}

	public static List<String> getOutgoingRequests(String uuid) {
		User user = Users.getUser(uuid);
		return user.getOutgoingRequests();
	}

	public static List<String> getIncomingRequests(String uuid) {
		User user = Users.getUser(uuid);
		return user.getIncomingRequests();
	}

	public static void acceptRequest(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		if (getIncomingRequests(uuid).contains(targetuuid)) {
			User target = Users.getUser(targetuuid);
			target.addFriend(uuid);
			target.removeOutgoingRequest(uuid);

			user.addFriend(targetuuid);
			user.removeIncomingRequest(targetuuid);

			Util.message(target.getPlayer(), "&c" + user.getName() + "&e &aAccepted&e your friend request!");
			Util.message(user.getPlayer(), "&eYou've &aAccepted&e &c" + target.getName() + "&e's friend request.");
		}
	}

	public static void declineRequest(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		if (getIncomingRequests(uuid).contains(targetuuid)) {
			User target = Users.getUser(targetuuid);
			target.removeOutgoingRequest(uuid);
			user.removeIncomingRequest(targetuuid);

			Util.message(target.getPlayer(), "&c" + user.getName() + "&e &4Declined&e your friend request!");
			Util.message(user.getPlayer(), "&eYou've &4Declined&e &c" + target.getName() + "&e's friend request.");
		}
	}

	public static void removeFriend(String uuid, String targetuuid) {
		User user = Users.getUser(uuid);
		User target = Users.getUser(targetuuid);
		if (isFriends(uuid, targetuuid)) {
			user.removeFriend(targetuuid);
			target.removeFriend(uuid);
			Util.message(user.getPlayer(), "&eYou've removed &c" + target.getName() + "&e from your friend list.");
			Util.message(target.getPlayer(), "&c" + user.getName() + "&e removed you from their friend list.");
		} else {
			Util.message(user.getPlayer(), "&4ERROR: &7" + target.getName() + " is not your friend.");
		}
	}
}
