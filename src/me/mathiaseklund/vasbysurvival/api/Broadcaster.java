package me.mathiaseklund.vasbysurvival.api;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Broadcaster {

	public static void startInfoLoop() {
		Util.debug("Starting Info Broadcast Loop");
		info();
	}

	static void info() {
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getMain(), new Runnable() {
			public void run() {
				List<String> info = Main.getMain().getMessages().getStringList("broadcast.info");
				int r = ThreadLocalRandom.current().nextInt(0, info.size());
				String message = info.get(r);
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
				info();
			}
		}, 20 * 60 * 3);
	}
}
