package me.mathiaseklund.vasbysurvival.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.objects.Report;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.InventoryUtil;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;

public class Reports {

	static HashMap<String, Report> reports = new HashMap<String, Report>();
	static HashMap<String, String> kicking = new HashMap<String, String>(); // ex. "playerUUID", "reportId"
	public static HashMap<String, String> banning = new HashMap<String, String>(); // ex. "playerUUID", "reportId"
	public static HashMap<String, String> tempbanning = new HashMap<String, String>(); // ex. "playerUUID", "reportId"
	public static HashMap<String, String> tempbanreason = new HashMap<String, String>(); // ex. "reportId", "reason"

	static HashMap<String, String> type = new HashMap<String, String>();
	static HashMap<String, Block> clickedblock = new HashMap<String, Block>();
	static HashMap<String, String> reportedplayer = new HashMap<String, String>();
	static ArrayList<String> reporting = new ArrayList<String>();
	public static ArrayList<String> reason = new ArrayList<String>();
	public static ArrayList<String> block = new ArrayList<String>();

	// TODO MAKE EVERYTHING CONFIGURABLE

	/**
	 * Open Report GUI
	 * 
	 * @param player
	 */
	public static void openReportGUI(Player player) {
		Inventory inv = Bukkit.createInventory(player, 18,
				ChatColor.translateAlternateColorCodes('&', "&4Report Type")); // TODO

		// SETUP BLOCK REPORT TYPE TODO
		ItemStack is = new ItemStack(Material.COBBLESTONE);
		ItemMeta im = is.getItemMeta();

		im.setDisplayName(
				Util.HideString("[reportgui.type]-block ") + ChatColor.translateAlternateColorCodes('&', "&eBlock"));
		List<String> lore = new ArrayList<String>();
		lore.add("Report a Block.");
		lore.add("Click this item and left click");
		lore.add("on the block you want to report.");
		im.setLore(lore);

		is.setItemMeta(im);
		inv.addItem(is);

		// SETUP PLAYER REPORT TYPE TODO
		is = new ItemStack(Material.PLAYER_HEAD);
		im = is.getItemMeta();
		im.setDisplayName(
				Util.HideString("[reportgui.type]-player ") + ChatColor.translateAlternateColorCodes('&', "&ePlayer"));
		lore = new ArrayList<String>();
		lore.add("Report a Player");
		im.setLore(lore);
		is.setItemMeta(im);
		inv.addItem(is);

		// SETUP REPORT CANCEL BUTTON TODO
		is = new ItemStack(Material.BARRIER);
		im = is.getItemMeta();
		im.setDisplayName(
				Util.HideString("[reportgui.cancel]") + ChatColor.translateAlternateColorCodes('&', "&cCancel Report"));
		is.setItemMeta(im);
		inv.setItem(inv.getSize() - 8, is);

		InventoryUtil.addFiller(inv);
		player.openInventory(inv);
		reporting.add(player.getUniqueId().toString());
	}

	/**
	 * Cancels users current report
	 * 
	 * @param player
	 */
	public static void cancelReport(Player player) {
		if (type.containsKey(player.getUniqueId().toString())) {
			type.remove(player.getUniqueId().toString());
		}
		if (clickedblock.containsKey(player.getUniqueId().toString())) {
			clickedblock.remove(player.getUniqueId().toString());
		}
		if (reportedplayer.containsKey(player.getUniqueId().toString())) {
			reportedplayer.remove(player.getUniqueId().toString());
		}
		if (reporting.contains(player.getUniqueId().toString())) {
			reporting.remove(player.getUniqueId().toString());
		}
		if (block.contains(player.getUniqueId().toString())) {
			block.remove(player.getUniqueId().toString());
		}
		if (reason.contains(player.getUniqueId().toString())) {
			reason.remove(player.getUniqueId().toString());
		}
		player.closeInventory();
		Util.message(player, Main.getMain().getMessages().getString("report.cancelled"));
	}

	public static void reportBlock(Player player) {
		type.put(player.getUniqueId().toString(), "block");
		block.add(player.getUniqueId().toString());
		Util.message(player, Main.getMain().getMessages().getString("report.block.click"));
		player.closeInventory();

	}

	public static void reportBlock(Player player, Block b) {
		block.remove(player.getUniqueId().toString());
		clickedblock.put(player.getUniqueId().toString(), b);
		reason.add(player.getUniqueId().toString());
		Util.message(player, Main.getMain().getMessages().getString("report.block.clicked"));
	}

	public static void reportPlayer(Player player) {
		type.put(player.getUniqueId().toString(), "player");
		openPlayerReportGUI(player);
	}

	public static void reportPlayer(Player player, String target) {
		reportedplayer.put(player.getUniqueId().toString(), target);
		reason.add(player.getUniqueId().toString());
		Util.message(player, Main.getMain().getMessages().getString("report.player.reason"));
		player.closeInventory();
	}

	public static void reportPlayer(Player p, String t, String r) {
		new Report(p, t, Bukkit.getPlayer(UUID.fromString(t)).getName(), r);
		Util.message(p, Main.getMain().getMessages().getString("report.submitted"));
		if (type.containsKey(p.getUniqueId().toString())) {
			type.remove(p.getUniqueId().toString());
		}
		if (clickedblock.containsKey(p.getUniqueId().toString())) {
			clickedblock.remove(p.getUniqueId().toString());
		}
		if (reportedplayer.containsKey(p.getUniqueId().toString())) {
			reportedplayer.remove(p.getUniqueId().toString());
		}
		if (reporting.contains(p.getUniqueId().toString())) {
			reporting.remove(p.getUniqueId().toString());
		}
		if (block.contains(p.getUniqueId().toString())) {
			block.remove(p.getUniqueId().toString());
		}
		if (reason.contains(p.getUniqueId().toString())) {
			reason.remove(p.getUniqueId().toString());
		}
	}

	public static void openPlayerReportGUI(Player player) {
		// TODO create player report GUI

		Inventory inv = Bukkit.createInventory(player, 54,
				ChatColor.translateAlternateColorCodes('&', "&4Report Player")); // TODO

		ItemStack is = new ItemStack(Material.BARRIER);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[reportgui.cancel]") + ChatColor.RED + "Cancel Report");
		is.setItemMeta(im);

		inv.setItem(inv.getSize() - 8, is);

		for (Player p : Bukkit.getOnlinePlayers()) {
			is = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta sm = (SkullMeta) is.getItemMeta();
			sm.setOwningPlayer(Bukkit.getPlayer(p.getName()));
			sm.setDisplayName(Util.HideString("[reportgui.player] " + p.getUniqueId().toString() + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&cReport " + p.getName()));
			is.setItemMeta(sm);
			inv.addItem(is);
		}

		InventoryUtil.addFiller(inv);
		player.openInventory(inv);
	}

	public static boolean isReporting(Player player) {
		String uuid = player.getUniqueId().toString();
		if (reporting.contains(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	public static Block getClickedBlock(Player player) {
		return clickedblock.get(player.getUniqueId().toString());
	}

	public static String getType(Player player) {
		return type.get(player.getUniqueId().toString());
	}

	public static void reportBlock(Player p, Block b, String r) {
		new Report(p, b, r);
		Util.message(p, Main.getMain().getMessages().getString("report.submitted"));
		if (type.containsKey(p.getUniqueId().toString())) {
			type.remove(p.getUniqueId().toString());
		}
		if (clickedblock.containsKey(p.getUniqueId().toString())) {
			clickedblock.remove(p.getUniqueId().toString());
		}
		if (reportedplayer.containsKey(p.getUniqueId().toString())) {
			reportedplayer.remove(p.getUniqueId().toString());
		}
		if (reporting.contains(p.getUniqueId().toString())) {
			reporting.remove(p.getUniqueId().toString());
		}
		if (block.contains(p.getUniqueId().toString())) {
			block.remove(p.getUniqueId().toString());
		}
		if (reason.contains(p.getUniqueId().toString())) {
			reason.remove(p.getUniqueId().toString());
		}
	}

	public static void notifyStaff() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			User u = Users.getUser(p.getUniqueId().toString());
			if (u.isStaff()) {
				Util.message(p, Main.getMain().getMessages().getString("report.notification"));
			}
		}
	}

	public static String getTargetPlayer(Player player) {
		return reportedplayer.get(player.getUniqueId().toString());
	}

	public static void openReportsGUI(Player player) {
		Inventory inv = Bukkit.createInventory(player, 54, ChatColor.translateAlternateColorCodes('&', "&4Reports"));

		for (String s : reports.keySet()) {
			Report report = reports.get(s);

			if (report.getBlock() != null) {
				Block b = report.getBlock();
				ItemStack is = new ItemStack(b.getType());
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(Util.HideString("[reportsgui.report]-" + report.getId() + " ")
						+ ChatColor.translateAlternateColorCodes('&', "&c" + report.getId()));
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.YELLOW + "Reporter: " + ChatColor.GRAY + report.getReporterName());
				lore.add(ChatColor.translateAlternateColorCodes('&',
						"&eLocation: &7" + Util.LocationToString(b.getLocation())));
				lore.add(ChatColor.translateAlternateColorCodes('&', "&eREASON"));
				lore.add(ChatColor.GRAY + report.getReason());
				im.setLore(lore);
				is.setItemMeta(im);
				inv.addItem(is);
			} else if (report.getTargetName() != null) {

				ItemStack is = new ItemStack(Material.PLAYER_HEAD);
				SkullMeta im = (SkullMeta) is.getItemMeta();
				im.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString(report.getTargetUUID())));
				im.setDisplayName(Util.HideString("[reportsgui.report]-" + report.getId() + " ")
						+ ChatColor.translateAlternateColorCodes('&', "&c" + report.getId()));
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.YELLOW + "Reporter: " + ChatColor.GRAY + report.getReporterName());
				lore.add(ChatColor.YELLOW + "Report Target: " + ChatColor.GRAY + report.getTargetName());
				lore.add(ChatColor.translateAlternateColorCodes('&', "&eREASON"));
				lore.add(ChatColor.GRAY + report.getReason());
				im.setLore(lore);
				is.setItemMeta(im);
				inv.addItem(is);
			}
		}

		InventoryUtil.addFiller(inv);

		player.openInventory(inv);
	}

	public static void addReport(String reportId, Report report) {
		reports.put(reportId, report);
	}

	public static void loadStoredReports() {
		File folder = new File(Main.getMain().getDataFolder() + File.separator + "reports" + File.separator);
		if (folder.exists()) {
			File[] files = folder.listFiles();
			for (File file : files) {
				Util.debug("Found report: " + file.getName());
				FileConfiguration f = new YamlConfiguration();
				try {
					f.load(file);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					new Report(file.getName().replaceAll(".yml", ""), f.getString("reporter.uuid"),
							f.getString("reporter.name"), f.getString("block"), f.getString("target.uuid"),
							f.getString("target.name"), f.getString("reason"));
				}
			}
		}
	}

	public static void openReportActionGUI(Player player, String reportId) {
		Inventory inv = Bukkit.createInventory(player, 54,
				ChatColor.translateAlternateColorCodes('&', "&cReport Action&7 | &c" + reportId));

		Report report = reports.get(reportId);
		if (report.getBlock() != null) {
			// Report is for block

			// Teleport Action
			ItemStack is = new ItemStack(Material.MAP);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.teleport]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eTeleport To"));
			is.setItemMeta(im);
			inv.addItem(is);

			// Remove Protection action
			is = new ItemStack(Material.BARRIER);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.removeprotection]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eRemove Protection"));
			is.setItemMeta(im);
			inv.addItem(is);

			// Break Block action
			is = new ItemStack(Material.IRON_PICKAXE);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.breakblock]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&cBreak Block"));
			is.setItemMeta(im);
			inv.addItem(is);

			// Block logs
			is = new ItemStack(Material.BOOK);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.blocklogs]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&cBlock Logs"));
			is.setItemMeta(im);
			inv.addItem(is);
		} else if (report.getTargetName() != null) {
			// Report is for player

			// Teleport Action
			ItemStack is = new ItemStack(Material.MAP);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.teleport]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eTeleport To "));
			is.setItemMeta(im);
			inv.addItem(is);

			// Kick Target action
			is = new ItemStack(Material.IRON_BOOTS);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.kick]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eKick Target"));
			is.setItemMeta(im);
			inv.addItem(is);

			// Temp Ban Target action
			is = new ItemStack(Material.BARRIER);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.tempban]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eTemp Ban Target"));
			is.setItemMeta(im);
			inv.addItem(is);

			// Ban Target action
			is = new ItemStack(Material.BARRIER);
			im = is.getItemMeta();
			im.setDisplayName(Util.HideString("[reportsgui.ban]-" + reportId + " ")
					+ ChatColor.translateAlternateColorCodes('&', "&eBan Target"));
			is.setItemMeta(im);
			inv.addItem(is);

		}

		// Close Report action
		ItemStack is = new ItemStack(Material.PAPER);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[reportsgui.close]-" + reportId + " ") + ChatColor.GOLD + "Close Report");
		is.setItemMeta(im);
		inv.addItem(is);

		InventoryUtil.addFiller(inv);
		player.openInventory(inv);
	}

	public static void getBlockLogs(Player player, String reportId) {
		Report report = reports.get(reportId);
		String location = Util.LocationToString(report.getBlock().getLocation());
		List<String> logs = Logs.getBlockLogs(location);

		ItemStack is = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta im = (BookMeta) is.getItemMeta();
		im.setAuthor(reportId);
		im.setDisplayName("Logs " + location);
		im.setTitle("Block Logs");
		if (logs != null) {
			for (String s : logs) {

				im.addPage(ChatColor.translateAlternateColorCodes('&', s));
			}
		} else {
			im.addPage("No Logs for Block");
		}
		is.setItemMeta(im);

		player.getInventory().addItem(is);
	}

	public static void closeReport(Player player, String reportId) {
		File file = new File(Main.getMain().getDataFolder() + File.separator + "reports" + File.separator,
				reportId + ".yml");
		if (file.exists()) {
			file.delete();
			reports.remove(reportId);
			Util.message(player, "&eYou have &cclosed&e the report with Id: &c" + reportId);
		}
		openReportsGUI(player);
	}

	public static void teleport(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getBlock() != null) {
			player.teleport(report.getBlock().getLocation());
		} else if (report.getTargetUUID() != null) {
			player.teleport(Bukkit.getPlayer(UUID.fromString(report.getTargetUUID())).getLocation());
		} else {
			Util.debug("No location found for report.");
		}
	}

	public static void removeProtection(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getBlock() != null) {
			Block b = report.getBlock();
			if (b.hasMetadata("owner")) {
				b.removeMetadata("owner", Main.getMain());
				Util.message(player, "&eYou've removed Protection from the reported block!");
				closeReport(player, reportId);
			}
		} else {
			Util.message(player, "&4ERROR:&7 Reported Block is NULL");
		}
	}

	public static void breakBlock(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getBlock() != null) {
			report.getBlock().breakNaturally();
			closeReport(player, reportId);
		} else {
			Util.message(player, "&4ERROR:&7 Reported Block is NULL");
		}
	}

	public static void kickTarget(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			kicking.put(player.getUniqueId().toString(), reportId);
			player.closeInventory();
			Util.message(player,
					"&cPlease write the reason for kicking target player in chat. &eType 'cancel' to cancel kick.");
		}
	}

	public static void banTarget(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			banning.put(player.getUniqueId().toString(), reportId);
			player.closeInventory();
			Util.message(player,
					"&cPlease write the reason for banning target player in chat. &eType 'cancel' to cancel ban.");
		}
	}

	public static void tempBanTarget(Player player, String reportId) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			tempbanning.put(player.getUniqueId().toString(), reportId);
			player.closeInventory();
			Util.message(player,
					"&cPlease write the reason for temporarily banning target player in chat. &eType 'cancel' to cancel ban.");
		}
	}

	public static void tempBanTarget(Player player, String reportId, String reason) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			tempbanreason.put(reportId, reason);
			player.closeInventory();
			Util.message(player, "&cPlease type the amount of seconds to ban target for. &e3600 = 1 Hour");
		}
	}

	public static void tempBanTarget(Player player, String reportId, String reason, int seconds) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			User user = Users.getUser(report.getTargetUUID());
			user.ban(reason, seconds);
			Util.message(player,
					"&cYou have banned " + report.getTargetName() + " for " + seconds + " Seconds. Reason: " + reason);
			closeReport(player, reportId);
		}
	}

	public static boolean isKicking(Player player) {
		if (kicking.containsKey(player.getUniqueId().toString())) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isTempBanning(Player player) {
		if (tempbanning.containsKey(player.getUniqueId().toString())) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean hasTempBanReason(Player player) {
		String reportId = tempbanning.get(player.getUniqueId().toString());
		if (tempbanreason.containsKey(reportId)) {
			return true;
		} else {
			return false;
		}
	}

	public static void cancelTempBan(Player player) {
		tempbanning.remove(player.getUniqueId().toString());
		tempbanreason.remove(player.getUniqueId().toString());
		Util.message(player, "&eCancelled temp ban process!");
	}

	public static void cancelBan(Player player) {
		banning.remove(player.getUniqueId().toString());
		Util.message(player, "&eCancelled ban process!");
	}

	public static String getKicking(Player player) {
		return kicking.get(player.getUniqueId().toString());
	}

	public static void cancelKick(Player player) {
		kicking.remove(player.getUniqueId().toString());
		Util.message(player, "&eYou've cancelled report response.");
	}

	public static void kickTarget(Player player, String reportId, String reason) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			Player target = Bukkit.getPlayer(UUID.fromString(report.getTargetUUID()));
			Bukkit.getScheduler().runTask(Main.getMain(), new Runnable() {
				public void run() {
					target.kickPlayer(reason);
				}
			});

			Util.message(player, "&eYou've kicked " + report.getTargetName() + " for reaosn: &c" + reason);
			closeReport(player, reportId);
		} else {
			Util.message(player, "&4ERROR:&7 Report does not have a target player.");
		}
	}

	public static String getTempBanReason(String reportId) {
		return tempbanreason.get(reportId);
	}

	public static boolean isBanning(Player player) {
		if (banning.containsKey(player.getUniqueId().toString())) {
			return true;
		} else {
			return false;
		}
	}

	public static void banTarget(Player player, String reportId, String reason) {
		Report report = reports.get(reportId);
		if (report.getTargetUUID() != null) {
			User user = Users.getUser(report.getTargetUUID());
			user.ban(reason, 0);
			Util.message(player, "&cYou have banned " + report.getTargetName() + ". Reason: " + reason);
			closeReport(player, reportId);
		}
	}
}
