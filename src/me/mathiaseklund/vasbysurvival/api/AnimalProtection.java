package me.mathiaseklund.vasbysurvival.api;

import java.util.ArrayList;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.util.Util;

public class AnimalProtection {

	public static ArrayList<String> protectinganimal = new ArrayList<String>();

	public void protectAnimal(Player player, LivingEntity ent) {
		if (protectinganimal.contains(player.getName())) {
			if (!hasOwner(ent)) {
				Util.debug("Fix animal protection");
			} else {
				Util.message(player, "&4ERROR:&7 Selected Animal already has an Owner.");
			}
		}
	}

	public boolean hasOwner(LivingEntity ent) {
		if (ent.hasMetadata("owner")) {
			return true;
		} else {
			return false;
		}
	}
}
