package me.mathiaseklund.vasbysurvival.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Logs {

	// Example: "locationStirng", ["log data","log data"]
	public static HashMap<String, List<String>> locationLogs = new HashMap<String, List<String>>();

	public static HashMap<String, List<String>> sessionblocks = new HashMap<String, List<String>>();

	public static void logBlockPlace(Player player, Block block) {
		String uuid = player.getUniqueId().toString();
		String name = player.getName();
		String type = block.getType().toString();
		String location = Util.LocationToString(block.getLocation());
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:m:s");
		String date = format.format(Calendar.getInstance().getTime());

		File file = new File(Main.getMain().getDataFolder() + File.separator + "logs" + File.separator,
				location + ".yml");
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				FileConfiguration f = new YamlConfiguration();
				try {
					f.load(file);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					List<String> logs = new ArrayList<String>();
					if (f.getStringList("logs") != null) {
						logs = f.getStringList("logs");
					}
					String log = "[&c" + date + "&r] " + uuid + " &c" + name + "&r PLACED &c" + type;
					logs.add(log);
					Util.debug(log);
					f.set("logs", logs);
					try {
						f.save(file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						Util.debug("Saved new log entry.");
					}
				}
			}
		} else {
			FileConfiguration f = new YamlConfiguration();
			try {
				f.load(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				List<String> logs = new ArrayList<String>();
				if (f.getStringList("logs") != null) {
					logs = f.getStringList("logs");
				}
				String log = "[&c" + date + "&r] " + uuid + " &c" + name + "&r PLACED &c" + type;
				logs.add(log);
				Util.debug(log);
				f.set("logs", logs);
				try {
					f.save(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					Util.debug("Saved new log entry.");
				}
			}
		}
	}

	public static void logBlockBreak(Player player, Block block) {
		String uuid = player.getUniqueId().toString();
		String name = player.getName();
		String type = block.getType().toString();
		String location = Util.LocationToString(block.getLocation());
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:m:s");
		String date = format.format(Calendar.getInstance().getTime());

		File file = new File(Main.getMain().getDataFolder() + File.separator + "logs" + File.separator,
				location + ".yml");
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				FileConfiguration f = new YamlConfiguration();
				try {
					f.load(file);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					List<String> logs = new ArrayList<String>();
					if (f.getStringList("logs") != null) {
						logs = f.getStringList("logs");
					}
					String log = "[&c" + date + "&r] " + uuid + " &c" + name + "&r BROKE &c" + type;
					Util.debug(log);
					logs.add(log);
					f.set("logs", logs);
					try {
						f.save(file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						Util.debug("Saved new log entry.");
					}
				}
			}
		} else {
			FileConfiguration f = new YamlConfiguration();
			try {
				f.load(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				List<String> logs = new ArrayList<String>();
				if (f.getStringList("logs") != null) {
					logs = f.getStringList("logs");
				}
				String log = "[&c" + date + "&r] " + uuid + " &c" + name + "&r BROKE &c" + type;
				Util.debug(log);
				logs.add(log);
				f.set("logs", logs);
				try {
					f.save(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					Util.debug("Saved new log entry.");
				}
			}
		}
	}

	public static List<String> getBlockLogs(String location) {
		File file = new File(Main.getMain().getDataFolder() + File.separator + "logs" + File.separator,
				location + ".yml");
		if (file.exists()) {
			FileConfiguration f = new YamlConfiguration();

			try {
				f.load(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<String> logs = f.getStringList("logs");
			return logs;
		} else {
			return null;
		}
	}

	public static void addSessionBlock(String uuid, Block b, String t) {
		String type = b.getType().toString();
		String location = Util.LocationToString(b.getLocation());
		List<String> list = sessionblocks.get(uuid);
		if (list == null) {
			list = new ArrayList<String>();
		}
		list.add(t + "  " + type + "  " + location);
		sessionblocks.put(uuid, list);
	}

	public static void rollback(String uuid) {
		if (sessionblocks.containsKey(uuid)) {
			for (int i = sessionblocks.get(uuid).size(); --i >= 0;) {
				String s = sessionblocks.get(uuid).get(i);
				String t = s.split("  ")[0];
				String type = s.split("  ")[1];
				String location = s.split("  ")[2];

				Location loc = Util.StringToLocation(location);
				if (t.equalsIgnoreCase("break")) {
					if (loc.getBlock().getType() == Material.AIR) {
						loc.getBlock().setType(Material.getMaterial(type));
						loc.getBlock().getState().update();
					}
				} else if (t.equalsIgnoreCase("place")) {
					if (loc.getBlock().getType().toString().equalsIgnoreCase(type)) {
						User user = Users.getUser(uuid);
						user.removeProtection(loc.getBlock());
						loc.getBlock().setType(Material.AIR);
						loc.getBlock().getState().update();
					}
				}
			}
		}
	}
}
