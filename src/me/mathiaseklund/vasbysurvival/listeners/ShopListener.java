package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class ShopListener implements Listener {

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		User user = Users.getUser(player.getUniqueId().toString());
		if (user.getCurrentShop() != null) {
			user.setCurrentShop(null);
			// TODO check if player is viewing shop content page
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			User user = Users.getUser(player.getUniqueId().toString());
			String shopId = user.getCurrentShop();
			if (shopId != null) {
				event.setCancelled(true);
				int slot = event.getRawSlot();
				Util.debug("Clicked Shop slot: " + slot);
			}
		}
	}
}
