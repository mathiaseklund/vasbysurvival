package me.mathiaseklund.vasbysurvival.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Logs;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class StaffListener implements Listener {

	Main main = Main.getMain();

	ArrayList<String> checkedowner = new ArrayList<String>();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.inStaffMode()) {
				event.getItemDrop().remove();
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());

			Logs.addSessionBlock(user.getId(), event.getBlock(), "break");
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.inStaffMode()) {
				event.setCancelled(true);
			} else {
				Logs.addSessionBlock(user.getId(), event.getBlock(), "place");
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onItemPickup(EntityPickupItemEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.inStaffMode()) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onRightClickBlock(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (!checkedowner.contains(player.getName())) {
			checkedowner.add(player.getName());
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block b = event.getClickedBlock();
				User user = Users.getUser(player.getUniqueId().toString());
				if (user.inStaffMode()) {
					if (b.hasMetadata("owner")) {

						if (!b.getMetadata("owner").isEmpty()) {
							String owner = b.getMetadata("owner").get(0).asString();
							Util.debug(owner);
							Util.message(player, "&7Block Owner: &c" + Users.getUser(owner).getName());
						} else {
							Util.message(player, "&7Block Owner:&c NULL");
						}
					} else {
						Util.message(player, "&7Block Owner:&c NULL");
					}
					event.setCancelled(true);
				}
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					checkedowner.remove(player.getName());
				}
			}, 10);
		}

	}
}
