package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		String command = event.getMessage();
		Util.debug("Command: " + command);
		if (command.contains("/pl") || command.contains("/me") || command.contains("/help") || command.contains("?")) {
			event.setCancelled(true);
		}
	}
}
