package me.mathiaseklund.vasbysurvival.listeners;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Interact;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;

public class PlayerListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		User user = Users.getUser(event.getPlayer().getUniqueId().toString());
		// event.setJoinMessage(null);

		if (user.hasGrace()) {
			long graceEnd = user.getGraceEnd();
			long now = Calendar.getInstance().getTimeInMillis();
			int seconds = (int) ((graceEnd - now) / 1000);
			Util.message(event.getPlayer(),
					main.getMessages().getString("grace.remaining").replaceAll("%grace%", seconds + ""));
		}
		user.motd();
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		User user = Users.getUser(event.getPlayer().getUniqueId().toString());
		user.store();
		Users.removeUser(event.getPlayer().getUniqueId().toString());
		// event.setQuitMessage(null);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if (!event.isCancelled()) {
			event.setCancelled(true);
			String message = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
			Player player = event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());

			message = user.getRolePrefix() + user.getNameColor() + user.getName() + " &7&l>>&r "
					+ user.getMessageColor() + message;
			if (!user.isMuted()) {
				Bukkit.broadcastMessage(ChatColor.GRAY + "");
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
			} else {
				Util.message(player, "&cYou are muted!");
			}

			// TODO add hover event to names and ranks.
		}
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		User user = new User(player.getUniqueId().toString(), event.getPlayer().getName());
		if (user.isBanned()) {
			long unbanon = user.getUnbanOn();
			if (unbanon > 0) {
				long now = Calendar.getInstance().getTimeInMillis();
				if (now >= unbanon) {
					event.allow();
					user.setBanned(false);
				} else {
					int remaining = (int) (unbanon - now) / 1000;
					event.disallow(Result.KICK_BANNED, "You have been banned. Remaining: " + remaining
							+ " Seconds | REASON: " + user.getBanReason());
				}
			} else {
				event.disallow(Result.KICK_BANNED, "You have been banned. | REASON: " + user.getBanReason());
			}
		}
	}

	@EventHandler
	public void onRightClickPlayer(PlayerInteractEntityEvent event) {
		Player p = event.getPlayer();
		if (event.getRightClicked() instanceof Player) {
			Player t = (Player) event.getRightClicked();
			event.setCancelled(true);
			Interact.openInteractMenu(p, t);
		}
	}
}