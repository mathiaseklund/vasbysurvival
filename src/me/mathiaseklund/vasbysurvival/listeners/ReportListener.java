package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Reports;
import me.mathiaseklund.vasbysurvival.util.Util;

public class ReportListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			ItemStack is = event.getCurrentItem();
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.hasDisplayName()) {
						String name = im.getDisplayName();
						name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
						Util.debug(name);
						if (name.contains("[reportgui.cancel]")) {
							Reports.cancelReport(player);
							event.setCancelled(true);
						} else if (name.contains("[reportgui.type]-")) {
							String type = name.split(" ")[0].split("-")[1];
							Util.debug("Clicked on report type: " + type);
							if (type.equalsIgnoreCase("block")) {
								Reports.reportBlock(player);
								event.setCancelled(true);
							} else if (type.equalsIgnoreCase("player")) {
								Reports.reportPlayer(player);
								event.setCancelled(true);
							}
						} else if (name.contains("[reportgui.player]")) {
							String t = name.split(" ")[1];
							Util.debug(t);
							Reports.reportPlayer(player, t);
							event.setCancelled(true);

						} else if (name.contains("[reportgui.filler]")) {
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.report]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.openReportActionGUI(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.teleport]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.teleport(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.removeprotection]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.removeProtection(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.breakblock]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.breakBlock(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.kick]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.kickTarget(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.tempban]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.tempBanTarget(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.ban]")) {
							String reportId = name.split(" ")[0].split("-")[1];
							Reports.banTarget(player, reportId);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.close]")) {
							Reports.closeReport(player, name.split(" ")[0].split("-")[1]);
							event.setCancelled(true);
						} else if (name.contains("[reportsgui.blocklogs]")) {
							Reports.getBlockLogs(player, name.split(" ")[0].split("-")[1]);
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (Reports.isReporting(player)) {
			if (Reports.block.contains(player.getUniqueId().toString())) {
				if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
					Reports.reportBlock(player, event.getClickedBlock());
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		if (Reports.isReporting(player)) {
			if (message.equalsIgnoreCase("cancel")) {
				Reports.cancelReport(player);
				event.setCancelled(true);
			} else {
				if (Reports.reason.contains(player.getUniqueId().toString())) {
					if (Reports.getType(player).equalsIgnoreCase("block")) {
						Reports.reportBlock(player, Reports.getClickedBlock(player), message);
						event.setCancelled(true);
					} else if (Reports.getType(player).equalsIgnoreCase("player")) {
						Reports.reportPlayer(player, Reports.getTargetPlayer(player), message);
						event.setCancelled(true);
					}

				}
			}
		} else if (Reports.isKicking(player)) {
			event.setCancelled(true);
			if (message.equalsIgnoreCase("cancel")) {
				Reports.cancelKick(player);
			} else {
				Reports.kickTarget(player, Reports.getKicking(player), message);
			}
		} else if (Reports.isTempBanning(player)) {
			event.setCancelled(true);
			if (message.equalsIgnoreCase("cancel")) {
				Reports.cancelTempBan(player);
			} else {
				if (Reports.hasTempBanReason(player)) {
					// Reason already set. Looking for time of ban
					if (Util.isInteger(message)) {
						String reportId = Reports.tempbanning.get(player.getUniqueId().toString());
						Reports.tempBanTarget(player, reportId, Reports.getTempBanReason(reportId),
								Integer.parseInt(message));
					} else {
						Util.message(player, "&4ERROR:&7 Ban length needs to be an Integer. Try again.");
					}
				} else {
					// Reason not set. Set reason
					String reportId = Reports.tempbanning.get(player.getUniqueId().toString());
					Reports.tempBanTarget(player, reportId, message);
				}
			}
		} else if (Reports.isBanning(player)) {
			Util.debug("Is banning a player");
			event.setCancelled(true);
			if (message.equalsIgnoreCase("cancel")) {
				Reports.cancelBan(player);
			} else {
				String reportId = Reports.banning.get(player.getUniqueId().toString());
				Reports.banTarget(player, reportId, message);
			}
		}
	}
}
