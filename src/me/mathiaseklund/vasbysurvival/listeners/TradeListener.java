package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Trades;
import me.mathiaseklund.vasbysurvival.util.Util;

public class TradeListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory inv = event.getView().getTopInventory();
		int rawslot = event.getRawSlot();
		String title = inv.getTitle();
		title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		if (title.contains("[tui]")) {
			String uuid = player.getUniqueId().toString();
			if (rawslot < 54) {
				if (event.getClick() == ClickType.LEFT || event.getClick() == ClickType.RIGHT) {
					if (rawslot == 2) {
						event.setCancelled(true);
						if (Trades.isSender(uuid)) {
							if (!Trades.hasAccepted(uuid)) {
								Trades.acceptTradeOffer(uuid);
							} else {
								Trades.declineTradeOffer(uuid);
							}
						}
					} else if (rawslot == 6) {
						event.setCancelled(true);
						if (Trades.isTarget(uuid)) {
							if (!Trades.hasAccepted(uuid)) {
								Trades.acceptTradeOffer(uuid);
							} else {
								Trades.declineTradeOffer(uuid);
							}
						}
					} else if (Trades.isSenderView(rawslot)) {
						if (Trades.isSender(uuid)) {
							Trades.forceDeclineOffer(uuid);
						} else {
							event.setCancelled(true);
						}
					} else if (Trades.isTargetView(rawslot)) {
						if (Trades.isTarget(uuid)) {
							Trades.forceDeclineOffer(uuid);
						} else {
							event.setCancelled(true);
						}
					} else {
						event.setCancelled(true);
					}
				} else {
					event.setCancelled(true);
				}
			} else {
				if (event.getClick() != ClickType.LEFT && event.getClick() != ClickType.RIGHT) {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onDrag(InventoryDragEvent event) {
		Inventory inv = event.getView().getTopInventory();
		String title = inv.getTitle();
		title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		if (title.contains("[tui]")) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onTradeClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		Inventory inv = event.getView().getTopInventory();
		String title = inv.getTitle();
		title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		Util.debug("Closing Inventory: " + title);
		if (title.contains("[tui]")) {
			String tradingwith = Trades.getTradePartner(player.getUniqueId().toString());
			if (tradingwith != null) {
				Trades.cancelTrade(player.getUniqueId().toString(), tradingwith, inv);
			}
		}
	}
}
