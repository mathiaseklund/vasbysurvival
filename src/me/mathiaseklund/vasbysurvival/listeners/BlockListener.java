package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class BlockListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		User user = Users.getUser(player.getUniqueId().toString());
		Block b = event.getBlock();
		if (!user.isOwner(b)) {
			if (user.isStaff()) {
				String owner = b.getMetadata("owner").get(0).asString();
				Users.getUser(owner).removeProtection(b);
				Util.message(player, "&aRemoved protected block");
			} else {
				event.setCancelled(true);
				Util.message(player, "&4Block is protected!");
			}
		} else {
			user.removeProtection(b);
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		User user = Users.getUser(player.getUniqueId().toString());
		Block b = event.getBlock();
		if (user.protectBlocks()) {
			// Protect placed block
			if (canBeProtected(b.getType().toString())) {
				user.protectBlock(b);
			}
		} else {
			// Don't protect placed block
		}

	}

	@EventHandler
	public void onRightClickBlock(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			if (b.getType().toString().contains("DOOR")) {
				Util.debug("Tried to right click a door.");
				Block bot;
				Block top;
				if (b.getRelative(BlockFace.UP).getType().toString().contains("DOOR")) {
					bot = b;
					top = b.getRelative(BlockFace.UP);
				} else {
					bot = b.getRelative(BlockFace.DOWN);
					top = b;
				}
				if (bot.hasMetadata("owner")) {
					String owner = bot.getMetadata("owner").get(0).asString();
					User user = Users.getUser(owner);
					if (!owner.equalsIgnoreCase(player.getUniqueId().toString())) {
						if (!Users.getUser(owner).isFriends(player.getUniqueId().toString())) {
							if (!user.isStaff()) {
								event.setCancelled(true);
							} else {
								if (!user.inStaffMode()) {
									event.setCancelled(true);
								}
							}
						}
					}
				} else if (top.hasMetadata("owner")) {
					String owner = top.getMetadata("owner").get(0).asString();
					User user = Users.getUser(owner);
					if (!owner.equalsIgnoreCase(player.getUniqueId().toString())) {
						if (!Users.getUser(owner).isFriends(player.getUniqueId().toString())) {
							if (!user.isStaff()) {
								event.setCancelled(true);
							} else {
								if (!user.inStaffMode()) {
									event.setCancelled(true);
								}
							}
						}
					}
				}
			} else if (b.getType().toString().contains("CHEST") || b.getType().toString().contains("FURNACE")
					|| b.getType().toString().contains("BOX") || b.getType().toString().contains("DISPENSER")
					|| b.getType().toString().contains("HOPPER")) {
				if (b.hasMetadata("owner")) {
					String owner = b.getMetadata("owner").get(0).asString();
					User user = Users.getUser(owner);
					if (!owner.equalsIgnoreCase(player.getUniqueId().toString())) {
						if (!Users.getUser(owner).isFriends(player.getUniqueId().toString())) {
							if (!user.isStaff()) {
								Util.message(player, "&4This block is locked!");
								event.setCancelled(true);
							} else {
								Util.message(player, "&aAccessed protected block.");
							}
						}
					}
				}
			} else {
				if (player.getInventory().getItemInMainHand().getType().toString().contains("LAVA")
						|| player.getInventory().getItemInOffHand().getType().toString().contains("LAVA")) {
					if (Users.getUser(player.getUniqueId().toString()).checkGrace()) {
						Util.debug("Blocking Lava placement for: " + player.getName());
						event.setCancelled(true);
					}
				} else if (player.getInventory().getItemInMainHand().getType().toString().contains("FLINT_AND_STEEL")
						|| player.getInventory().getItemInOffHand().getType().toString().contains("FLINT_AND_STEEL")) {
					if (Users.getUser(player.getUniqueId().toString()).checkGrace()) {
						Util.debug("Blocking Fire placement for: " + player.getName());
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		Block to = event.getToBlock();
		if (to.getType() != Material.AIR) {
			if (to.hasMetadata("owner")) {
				event.setCancelled(true);

			}
		}
	}

	@EventHandler
	public void onLeftClickBlock(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			if (block.getType() == Material.SPAWNER) {
				if (!block.hasMetadata("owner")) {
					User user = Users.getUser(player.getUniqueId().toString());
					user.protectBlock(block);
					Util.message(player, "&aProtected clicked Spawner");
				}
			}
		}
	}

	public boolean canBeProtected(String mat) {
		if (mat.contains("SAPLING") || mat.contains("WATER") || mat.contains("LAVA") || mat.contains("FIRE")
				|| mat.contains("TNT")) {
			return false;
		} else {
			return true;
		}
	}
}
