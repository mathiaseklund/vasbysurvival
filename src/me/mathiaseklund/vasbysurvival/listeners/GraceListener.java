package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class GraceListener implements Listener {
	Main main = Main.getMain();

	@EventHandler
	public void onPlayerDamagePlayer(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player player = (Player) event.getEntity();
			Player damager = (Player) event.getDamager();

			User user = Users.getUser(player.getUniqueId().toString());
			if (user.hasGrace()) {
				Util.debug("attacked has grace.");
				if (user.checkGrace()) {
					Util.debug("Cancel attack");
					event.setCancelled(true);
				}
			}

			User dmger = Users.getUser(damager.getUniqueId().toString());

			if (dmger.hasGrace()) {
				Util.debug("Damager has grace.");
				if (user.hasGrace()) {
					dmger.forceEndGrace();
					Util.message(damager, main.getMessages().getString("grace.forceended"));
				} else {
					Util.debug("Cancel attack");
					event.setCancelled(true);
				}
			} else {
				Util.debug("Damager does not have grace.");
			}

		}
	}
}
