package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedEnterEvent.BedEnterResult;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;

public class SleepListener implements Listener {

	@EventHandler
	public void onPlayerSleep(PlayerBedEnterEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());
			if (event.getBedEnterResult() == BedEnterResult.OK) {
				if (user.isPremium()) {
					player.getWorld().setTime(0);
				}
			}
		}
	}
}
