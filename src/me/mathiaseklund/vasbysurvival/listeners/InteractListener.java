package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Friends;
import me.mathiaseklund.vasbysurvival.api.Trades;
import me.mathiaseklund.vasbysurvival.util.Util;

public class InteractListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			ItemStack is = event.getCurrentItem();
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.hasDisplayName()) {
						String name = im.getDisplayName();
						name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
						if (name.contains("[interact.addfriend]")) {
							event.setCancelled(true);
							Util.debug(player.getName() + " wants to add target as friend.");

							String target = name.split(" ")[1];
							Friends.addFriend(player.getUniqueId().toString(), target);
							player.closeInventory();
						} else if (name.contains("[interact.removefriend]")) {
							event.setCancelled(true);
							Util.debug(player.getName() + " wants to remove target from friendslist");
							String target = name.split(" ")[1];
							Friends.removeFriend(player.getUniqueId().toString(), target);
							player.closeInventory();
						} else if (name.contains("[interact.trade]")) {
							event.setCancelled(true);
							Util.debug(player.getName() + " wants to trade with target.");
							String target = name.split(" ")[1];
							Trades.requestTrade(player.getUniqueId().toString(), target);
							player.closeInventory();
						}
					}
				}
			}
		}
	}
}
