package me.mathiaseklund.vasbysurvival.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import me.mathiaseklund.vasbysurvival.Main;

public class EntityListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if(event.getEntityType() == EntityType.PHANTOM) {
			event.setCancelled(true);
		}
	}
}
