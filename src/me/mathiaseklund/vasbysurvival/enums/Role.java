package me.mathiaseklund.vasbysurvival.enums;

public enum Role {
	USER, PREMIUM, STAFF
}
