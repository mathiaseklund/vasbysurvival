package me.mathiaseklund.vasbysurvival.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Bed;
import org.bukkit.block.data.type.Bed.Part;
import org.bukkit.block.data.type.Door;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.enums.Role;
import me.mathiaseklund.vasbysurvival.util.Util;

public class User {

	Main main = Main.getMain();

	File file;
	FileConfiguration f;

	Player player;

	String uuid;
	String name;
	String nameColor;
	String messageColor;

	List<String> incoming_trade_requests = new ArrayList<String>();
	List<String> outgoing_trade_requests = new ArrayList<String>();

	List<String> friends = new ArrayList<String>();
	List<String> incoming_friend_requests = new ArrayList<String>();
	List<String> outgoing_friend_requests = new ArrayList<String>();
	List<Role> roles;
	boolean staffmode = false;

	boolean grace;
	long graceEnd;

	boolean protectblocks;

	boolean muted;
	long unmutedon;

	boolean banned;
	long unbanon;
	String banreason;

	HashMap<String, String> locations = new HashMap<String, String>(); // Example: "locationId", "locationString"
	HashMap<String, String> marks = new HashMap<String, String>(); // Example: "markId", "locationString"
	HashMap<String, List<ItemStack>> virtualchests = new HashMap<String, List<ItemStack>>();

	String currentvc;
	String currentShop;

	//////////////////////////////////////

	public User(String uuid) {
		this.uuid = uuid;
		this.player = Bukkit.getPlayer(UUID.fromString(uuid));
		Users.addUser(uuid, this);
		load();
	}

	public User(String uuid, String name) {
		this.uuid = uuid;
		this.name = name;
		this.player = Bukkit.getPlayer(UUID.fromString(uuid));
		Users.addUser(uuid, this);
		load();
	}

	///////////////////////////////////////

	public void load() {
		Util.debug("Loading " + uuid + " data file");
		file = new File(main.getDataFolder() + File.separator + "users" + File.separator, uuid + ".yml");
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		f = new YamlConfiguration();
		try {
			f.load(file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		} finally {
			// Load user name
			String name = f.getString("name");
			if (name != null) {
				this.name = name;
			}

			// Load color codes for name and messages
			String nameColor = f.getString("color.name");
			String messageColor = f.getString("color.message");
			if (nameColor == null) {
				this.nameColor = "&f";
			} else {
				this.nameColor = nameColor;
			}
			if (messageColor == null) {
				this.messageColor = "&f";
			} else {
				this.messageColor = messageColor;
			}

			// Load user roles
			List<Role> roles = new ArrayList<Role>();
			for (String s : f.getStringList("roles")) {
				Util.debug("Found User Role: " + s);
				roles.add(Role.valueOf(s));
			}
			if (roles.isEmpty()) {
				Util.debug("Roles empty. Adding default USER role");
				roles.add(Role.USER);
			}
			this.roles = roles;

			// Load user grace period
			if (f.getLong("grace") > 0) {
				this.graceEnd = f.getLong("grace");
				if (this.graceEnd <= Calendar.getInstance().getTimeInMillis()) {
					this.grace = false;
				} else {
					this.grace = true;
				}

			} else {
				// First time connecting.
				this.graceEnd = Calendar.getInstance().getTimeInMillis() + (1800 * 1000);
				this.grace = true;
			}

			// Load protect blocks boolean
			if (f.get("protectblocks") == null) {
				this.protectblocks = true;
			} else {
				this.protectblocks = f.getBoolean("protectblocks");
			}

			// Load staffmode boolean
			if (f.get("staffmode") == null) {
				this.staffmode = false;
			} else {
				this.staffmode = f.getBoolean("staffmode");
			}

			// Load stored locations
			if (f.getConfigurationSection("locations") != null) {
				for (String s : f.getConfigurationSection("locations").getKeys(false)) {
					locations.put(s, f.getString("locations." + s));
				}
			}

			// Load stored marks
			if (f.getConfigurationSection("marks") != null) {
				for (String s : f.getConfigurationSection("marks").getKeys(false)) {
					marks.put(s, f.getString("marks." + s));
				}
			}

			// Load ban data
			if (f.getBoolean("banned")) {
				this.banned = true;
				this.unbanon = f.getLong("unbanon");
				this.banreason = f.getString("banreason");
			}

			// Load muted data
			if (f.getBoolean("muted")) {
				this.muted = true;
				this.unmutedon = f.getLong("unmutedon");
			}
			if (f.get("friends.list") != null) {
				this.friends = f.getStringList("friends.list");
			}
			if (f.get("friends.outgoing") != null) {
				this.outgoing_friend_requests = f.getStringList("friends.outgoing");
			}
			if (f.get("friends.incoming") != null) {
				this.incoming_friend_requests = f.getStringList("friends.incoming");
			}
			loadVirtualChests();
			store();
		}
	}

	public void store() {
		f.set("name", this.name);
		List<String> roles = new ArrayList<String>();
		for (Role role : this.roles) {
			roles.add(role.toString());
		}
		f.set("color.name", this.nameColor);
		f.set("color.message", this.messageColor);
		f.set("roles", roles);
		f.set("grace", this.graceEnd);
		f.set("protectblocks", this.protectblocks);
		f.set("staffmode", this.staffmode);
		f.set("friends.list", this.friends);
		f.set("friends.incoming", this.incoming_friend_requests);
		f.set("friends.outgoing", this.outgoing_friend_requests);
		for (String s : locations.keySet()) {
			f.set("locations." + s, locations.get(s));
		}
		for (String s : marks.keySet()) {
			f.set("marks." + s, marks.get(s));
		}
		if (this.banned) {
			f.set("banned", this.banned);
			f.set("unbanon", this.unbanon);
			f.set("banreason", this.banreason);
		}
		if (this.muted) {
			f.set("muted", this.muted);
			f.set("unmutedon", this.unmutedon);
		}

		save();
	}

	public void save() {
		try {
			f.save(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/////////////////////////////////////

	public String getId() {
		return this.uuid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
		f.set(name, name);
		save();
	}

	public String getRolePrefix() {
		if (roles.contains(Role.STAFF)) {
			return "&cStaff&r ";
		} else if (roles.contains(Role.PREMIUM)) {
			return "&6✪&r ";
		} else {
			return "";
		}
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public boolean hasRole(Role role) {
		if (roles.contains(role)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isStaff() {
		if (roles.contains(Role.STAFF)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean inStaffMode() {
		return this.staffmode;
	}

	public void toggleStaffMode() {
		if (staffmode) {
			Util.debug("Turning staff mode off.");
			staffmode = false;
			getNormalInventory();
			if (locations.get("playermode") != null) {
				Location loc = Util.StringToLocation(locations.get("playermode"));
				player.teleport(loc);
			}
			player.setGameMode(GameMode.SURVIVAL);
			locations.remove("playermode");
		} else {
			Util.debug("Turning staff mode on.");
			staffmode = true;
			storeNormalInventory();
			getStaffInventory();
			player.setGameMode(GameMode.CREATIVE);
			String location = Util.LocationToString(player.getLocation());
			locations.put("playermode", location);
			f.set("locations.playermode", location);
		}
		f.set("staffmode", staffmode);
		save();
	}

	public boolean isPremium() {
		if (roles.contains(Role.PREMIUM)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasGrace() {
		return this.grace;
	}

	public long getGraceEnd() {
		return this.graceEnd;
	}

	public boolean checkGrace() {
		long now = Calendar.getInstance().getTimeInMillis();
		if (getGraceEnd() <= now) {
			grace = false;
			return false;
		} else {
			return true;
		}
	}

	public void forceEndGrace() {
		grace = false;
		graceEnd = Calendar.getInstance().getTimeInMillis();
	}

	public boolean protectBlocks() {
		return this.protectblocks;
	}

	public void toggleProtectBlocks() {
		this.protectblocks = !this.protectblocks;
		f.set("protectblocks", this.protectblocks);
		save();
	}

	public void getNormalInventory() {
		if (player == null) {
			player = Bukkit.getPlayer(UUID.fromString(this.uuid));
		}
		player.getInventory().clear();
		if (f.getConfigurationSection("inventory.content") != null) {
			for (String s : f.getConfigurationSection("inventory.content").getKeys(false)) {
				ItemStack is = f.getItemStack("inventory.content." + s);
				player.getInventory().setItem(Integer.parseInt(s), is);
			}
		}
		player.getInventory().setItemInOffHand(f.getItemStack("inventory.offhand"));
		player.getInventory().setHelmet(f.getItemStack("inventory.helmet"));
		player.getInventory().setChestplate(f.getItemStack("inventory.chest"));
		player.getInventory().setLeggings(f.getItemStack("inventory.leggings"));
		player.getInventory().setBoots(f.getItemStack("inventory.boots"));
	}

	public void storeNormalInventory() {
		if (player == null) {
			this.player = Bukkit.getPlayer(UUID.fromString(this.uuid));
		}
		ItemStack h = player.getInventory().getHelmet();
		ItemStack c = player.getInventory().getChestplate();
		ItemStack l = player.getInventory().getLeggings();
		ItemStack b = player.getInventory().getBoots();
		ItemStack o = player.getInventory().getItemInOffHand();

		for (int i = 0; i < player.getInventory().getSize(); i++) {
			f.set("inventory.content." + i, player.getInventory().getItem(i));
		}
		f.set("inventory.helmet", h);
		f.set("inventory.chest", c);
		f.set("inventory.leggings", l);
		f.set("inventory.boots", b);
		f.set("inventory.offhand", o);
		save();
	}

	public void getStaffInventory() {
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
	}

	public HashMap<String, String> getMarks() {
		return this.marks;
	}

	public String getMark(String markId) {
		return this.marks.get(markId);
	}

	public void addMark(String markId) {
		if (player == null) {
			this.player = Bukkit.getPlayer(UUID.fromString(this.uuid));
		}
		String location = Util.LocationToString(player.getLocation());
		this.marks.put(markId, location);
		f.set("marks." + markId, location);
		save();
	}

	public void removeMark(String markId) {
		this.marks.remove(markId);
		f.set("marks." + markId, null);
		save();
	}

	public boolean isBanned() {
		return this.banned;
	}

	public long getUnbanOn() {
		return this.unbanon;
	}

	public String getBanReason() {
		return this.banreason;
	}

	public void ban(String reason, int seconds) {
		if (seconds > 0) {
			long unbanon = Calendar.getInstance().getTimeInMillis() + (seconds * 1000);
			this.unbanon = unbanon;
		} else {
			this.unbanon = 0;
		}
		this.banreason = reason;
		this.banned = true;
		String uuid = this.uuid;
		Bukkit.getScheduler().runTask(main, new Runnable() {
			public void run() {
				if (seconds > 0) {
					Bukkit.getPlayer(UUID.fromString(uuid))
							.kickPlayer("You have been banned for " + seconds + " Seconds. REASON: " + reason);
				} else {
					Bukkit.getPlayer(UUID.fromString(uuid)).kickPlayer("You have been banned. REASON: " + reason);
				}
			}
		});
	}

	public void setBanned(boolean b) {
		this.banned = b;
	}

	public String getNameColor() {
		if (isPremium()) {
			return this.nameColor;
		} else {
			return "&f";
		}
	}

	public String getMessageColor() {
		if (isPremium()) {
			return this.messageColor;
		} else {
			return "&f";
		}
	}

	public void setMessageColor(String color) {
		this.messageColor = color;
		f.set("color.message", this.messageColor);
		save();
	}

	public void setNameColor(String color) {
		this.nameColor = color;
		f.set("color.name", this.nameColor);
		save();
	}

	public void addRole(String r) {
		Role role = Role.valueOf(r);
		if (!this.roles.contains(role)) {
			this.roles.add(role);

			List<String> roles = new ArrayList<String>();
			for (Role r1 : this.roles) {
				roles.add(r1.toString());
			}
			f.set("roles", roles);
			save();
		}
	}

	public void removeRole(String r) {
		Role role = Role.valueOf(r);
		if (this.roles.contains(role)) {
			this.roles.remove(role);

			List<String> roles = new ArrayList<String>();
			for (Role r1 : this.roles) {
				roles.add(r1.toString());
			}
			f.set("roles", roles);
			save();
		}
	}

	public void mute(int time) {
		if (time > 0) {
			this.unmutedon = Calendar.getInstance().getTimeInMillis() + (time * 1000);
			setMuted(true);
		} else {
			setMuted(true);
			this.unmutedon = 0;
		}
	}

	public void unmute() {
		this.muted = false;
	}

	public void setMuted(boolean b) {
		this.muted = b;
	}

	public boolean isMuted() {
		if (this.unmutedon > 0) {
			long now = Calendar.getInstance().getTimeInMillis();
			if (now >= this.unmutedon) {
				this.muted = false;
			}
		}
		return this.muted;
	}

	public List<String> getFriends() {
		return this.friends;
	}

	public boolean isFriends(String targetuuid) {

		return this.friends.contains(targetuuid);
	}

	public Player getPlayer() {
		if (this.player == null) {
			this.player = Bukkit.getPlayer(UUID.fromString(this.uuid));
		}
		return this.player;
	}

	public void addIncomingRequest(String uuid) {
		if (!this.incoming_friend_requests.contains(uuid)) {
			this.incoming_friend_requests.add(uuid);
		}
		f.set("friends.incoming", this.incoming_friend_requests);
		save();
	}

	public void addOutgoingRequest(String uuid) {
		if (!this.outgoing_friend_requests.contains(uuid)) {
			this.outgoing_friend_requests.add(uuid);
		}
		f.set("friends.outgoing", this.outgoing_friend_requests);
		save();
	}

	public void removeIncomingRequest(String uuid) {
		this.incoming_friend_requests.remove(uuid);
		f.set("friends.incoming", this.incoming_friend_requests);
		save();
	}

	public void removeOutgoingRequest(String uuid) {
		this.outgoing_friend_requests.remove(uuid);
		f.set("friends.outgoing", this.outgoing_friend_requests);
		save();
	}

	public List<String> getOutgoingRequests() {
		return this.outgoing_friend_requests;
	}

	public List<String> getIncomingRequests() {
		return this.incoming_friend_requests;
	}

	public void addFriend(String uuid) {
		this.friends.add(uuid);
		f.set("friends.list", this.friends);
		save();
	}

	public void removeFriend(String uuid) {
		this.friends.remove(uuid);
		f.set("friends.list", this.friends);
		save();
	}

	public List<String> getIncomingTradeRequests() {
		return this.incoming_trade_requests;
	}

	public List<String> getOutgoingTradeRequests() {
		return this.outgoing_trade_requests;
	}

	public void addIncomingTradeRequest(String target) {
		this.incoming_trade_requests.add(target);
	}

	public void addOutgoingTradeRequest(String target) {
		this.outgoing_trade_requests.add(target);
	}

	public void removeIncomingTradeRequest(String target) {
		this.incoming_trade_requests.remove(target);
	}

	public void removeOutgoingTradeRequest(String target) {
		this.outgoing_trade_requests.remove(target);
	}

	public void protectBlock(Block block) {
		List<String> blocks = f.getStringList("protected");
		String location = Util.LocationToString(block.getLocation());
		if (blocks == null) {
			blocks = new ArrayList<String>();
		}

		block.setMetadata("owner", new FixedMetadataValue(main, this.getId()));
		String type = block.getType().toString();
		if (type.contains("_BED")) {
			Bed bed = (Bed) block.getBlockData();
			if (bed.getPart() == Part.FOOT) {
				for (BlockFace face : bed.getFaces()) {
					Block b = block.getRelative(face);
					if (b.getType().toString().contains("_BED")) {
						Bed bed1 = (Bed) b.getBlockData();
						if (bed1.getPart() == Part.HEAD) {
							if (!b.hasMetadata("owner")) {
								b.setMetadata("owner", new FixedMetadataValue(main, this.getId()));
								if (!blocks.contains(Util.LocationToString(b.getLocation()))) {
									blocks.add(Util.LocationToString(b.getLocation()));
								}
								break;
							}
						}
					}
				}
			}
		}
		if (type.contains("_DOOR")) {
			Door door = (Door) block.getBlockData();
			if (door.getHalf() == Half.BOTTOM) {
				Block b = block.getRelative(BlockFace.UP);
				if (!b.hasMetadata("owner")) {
					b.setMetadata("owner", new FixedMetadataValue(main, this.getId()));
					if (!blocks.contains(Util.LocationToString(b.getLocation()))) {
						blocks.add(Util.LocationToString(b.getLocation()));
					}
				}
			}
		}
		if (!blocks.contains(location)) {
			blocks.add(location);
			f.set("protected", blocks);
			save();
		}
		save();
	}

	public void removeProtection(Block block) {
		block.removeMetadata("owner", main);
		String location = Util.LocationToString(block.getLocation());
		List<String> blocks = f.getStringList("protected");
		String type = block.getType().toString();
		if (type.contains("_BED")) {
			Bed bed = (Bed) block.getBlockData();
			if (bed.getPart() == Part.FOOT) {
				for (BlockFace face : bed.getFaces()) {
					Block b = block.getRelative(face);
					if (b.getType().toString().contains("_BED")) {
						Bed bed1 = (Bed) b.getBlockData();
						if (bed1.getPart() == Part.HEAD) {
							if (b.hasMetadata("owner")) {
								b.removeMetadata("owner", main);
								if (blocks.contains(Util.LocationToString(b.getLocation()))) {
									blocks.remove(Util.LocationToString(b.getLocation()));
								}
								break;
							}
						}
					}
				}
			} else if (bed.getPart() == Part.HEAD) {
				for (BlockFace face : bed.getFaces()) {
					Block b = block.getRelative(face);
					if (b.getType().toString().contains("_BED")) {
						Bed bed1 = (Bed) b.getBlockData();
						if (bed1.getPart() == Part.FOOT) {
							if (b.hasMetadata("owner")) {
								b.removeMetadata("owner", main);
								if (blocks.contains(Util.LocationToString(b.getLocation()))) {
									blocks.remove(Util.LocationToString(b.getLocation()));
								}
								break;
							}
						}
					}
				}
			}
		}
		if (type.contains("_DOOR")) {
			Door door = (Door) block.getBlockData();
			if (door.getHalf() == Half.BOTTOM) {
				Block b = block.getRelative(BlockFace.UP);
				if (b.hasMetadata("owner")) {
					b.removeMetadata("owner", main);
					if (blocks.contains(Util.LocationToString(b.getLocation()))) {
						blocks.remove(Util.LocationToString(b.getLocation()));
					}
				}
			} else if (door.getHalf() == Half.TOP) {
				Block b = block.getRelative(BlockFace.DOWN);
				if (b.hasMetadata("owner")) {
					b.removeMetadata("owner", main);
					if (blocks.contains(Util.LocationToString(b.getLocation()))) {
						blocks.remove(Util.LocationToString(b.getLocation()));
					}
				}
			}
		}

		blocks.remove(location);
		f.set("protected", blocks);
		save();
	}

	public boolean isOwner(Block b) {
		if (b.hasMetadata("owner")) {
			String owner = b.getMetadata("owner").get(0).asString();
			if (owner.equalsIgnoreCase(this.getId())) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	public Set<String> getVirtualChests() {
		return this.virtualchests.keySet();
	}

	public List<ItemStack> getVirtualChest(String chestId) {
		return this.virtualchests.get(chestId);
	}

	public boolean hasVirtualChest(String chestId) {
		if (this.virtualchests.containsKey(chestId)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean canCreateVirtualChest() {
		if (!this.virtualchests.isEmpty()) {
			if (this.isPremium()) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	public void storeVirtualChestItems(String chestId, Inventory inv) {
		List<ItemStack> items = new ArrayList<ItemStack>();
		int i = 0;
		Util.debug("Storing VC: " + chestId);
		for (ItemStack is : inv.getContents()) {
			f.set("virtualchest." + chestId + "." + i, is);
			save();
			i++;
			if (is != null) {
				items.add(is);
			}
		}
		this.virtualchests.put(chestId, items);
		if (i == 0) {
			f.set("virtualchest." + chestId, null);
			save();
			this.virtualchests.remove(chestId);
		}
		Util.debug("Stored VC Items");
	}

	public void loadVirtualChests() {
		if (f.getConfigurationSection("virtualchest") != null) {
			if (f.getConfigurationSection("virtualchest").getKeys(false) != null) {
				for (String key : f.getConfigurationSection("virtualchest").getKeys(false)) {
					List<ItemStack> items = new ArrayList<ItemStack>();
					for (int i = 0; i < 54; i++) {
						ItemStack is = f.getItemStack("virtualchest." + key + "." + i);
						if (is != null) {
							items.add(is);
						} else {
							break;
						}
					}
					this.virtualchests.put(key, items);
				}
			}
		}
	}

	public void createNewVirtualChest(String chestId) {
		this.virtualchests.put(chestId, new ArrayList<ItemStack>());
	}

	public void openVirtualChest(String chestId) {
		if (this.player == null) {
			this.player = Bukkit.getPlayer(UUID.fromString(this.getId()));
		}
		if (this.hasVirtualChest(chestId)) {
			Inventory inv = Bukkit.createInventory(null, 54, "Virtual Chest | " + chestId);
			for (ItemStack is : this.virtualchests.get(chestId)) {
				inv.addItem(is);
			}
			this.player.openInventory(inv);
			this.currentvc = chestId;
		}
	}

	public String getCurrentVC() {
		return this.currentvc;
	}

	public void setCurrentVC(String s) {
		this.currentvc = s;
	}

	public String getCurrentShop() {
		return this.currentShop;
	}

	public void setCurrentShop(String s) {
		this.currentShop = s;
	}

	public void motd() {
		List<String> motd = main.getMessages().getStringList("motd");
		Player p = getPlayer();
		if (p != null) {
			for (String s : motd) {
				Util.message(p, s);
			}
		}
	}
}
