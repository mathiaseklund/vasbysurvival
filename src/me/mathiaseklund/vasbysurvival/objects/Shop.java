package me.mathiaseklund.vasbysurvival.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;

public class Shop {

	Main main = Main.getMain();

	User owner;
	String shopId;

	Inventory shopPage;
	Inventory shopPageAdmin;

	ItemStack sellorders;
	ItemStack purchaseorders;
	ItemStack stock;

	List<ItemStack> sold = new ArrayList<ItemStack>();
	List<ItemStack> purchased = new ArrayList<ItemStack>();

	// CONSTRUCTOR ///////////////

	public Shop(String owner, String shopId, boolean created) {
		this.owner = Users.getUser(owner);
		this.shopId = shopId;
		if (created) {
			create();
		}
	}

	///////////////////////////////

	public void create() {
		Block b = owner.getPlayer().getTargetBlock(null, 5);
		if (b.getType() == Material.CHEST) {
			if (b.hasMetadata("owner")) {
				String blockowner = b.getMetadata("owner").get(0).asString();
				if (blockowner.equalsIgnoreCase(owner.getId())) {
					b.setMetadata("shop", new FixedMetadataValue(main, this.shopId));

					createShopPage();
					createShopAdmin();

					openShop(owner.getPlayer());
				} else {
					Util.message(owner.getPlayer(), "&4ERROR:&7 Target chest is not owned by you.");
				}
			} else {
				Util.message(owner.getPlayer(), "&4ERROR:&7 Target chest is not owned by you.");
			}
		} else {
			Util.message(owner.getPlayer(), "&4ERROR:&7 Target block is not a chest.");
		}
	}

	public void createShopPage() {
		shopPage = Bukkit.createInventory(null, 54, "Player Shop: " + shopId);
		shopPageAdmin = Bukkit.createInventory(null, 54,
				Util.HideString("[shop] " + this.shopId + " ") + "Player Shop: " + shopId);

		ItemStack is = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[s.f]"));
		is.setItemMeta(im);
		shopPage.setItem(0, is);
		shopPage.setItem(1, is);
		shopPage.setItem(2, is);
		shopPage.setItem(3, is);
		shopPage.setItem(4, is);
		shopPage.setItem(5, is);
		shopPage.setItem(6, is);
		shopPage.setItem(7, is);
		shopPage.setItem(8, is);

		shopPage.setItem(9, is);
		shopPage.setItem(10, is);
		shopPage.setItem(11, is);
		shopPage.setItem(12, is);
		shopPage.setItem(13, is);
		shopPage.setItem(14, is);
		shopPage.setItem(15, is);
		shopPage.setItem(16, is);
		shopPage.setItem(17, is);

		shopPage.setItem(18, is);
		shopPage.setItem(22, is);
		shopPage.setItem(26, is);

		shopPage.setItem(27, is);
		shopPage.setItem(31, is);
		shopPage.setItem(35, is);

		shopPage.setItem(36, is);
		shopPage.setItem(40, is);
		shopPage.setItem(44, is);

		shopPage.setItem(45, is);
		shopPage.setItem(46, is);
		shopPage.setItem(47, is);
		shopPage.setItem(48, is);
		shopPage.setItem(49, is);
		shopPage.setItem(50, is);
		shopPage.setItem(51, is);
		shopPage.setItem(52, is);
		shopPage.setItem(53, is);

		shopPageAdmin.setItem(0, is);
		shopPageAdmin.setItem(1, is);
		shopPageAdmin.setItem(2, is);
		shopPageAdmin.setItem(3, is);
		shopPageAdmin.setItem(4, is);
		shopPageAdmin.setItem(5, is);
		shopPageAdmin.setItem(6, is);
		shopPageAdmin.setItem(7, is);
		shopPageAdmin.setItem(8, is);

		shopPageAdmin.setItem(9, is);
		shopPageAdmin.setItem(10, is);
		shopPageAdmin.setItem(11, is);
		shopPageAdmin.setItem(12, is);
		shopPageAdmin.setItem(13, is);
		shopPageAdmin.setItem(14, is);
		shopPageAdmin.setItem(15, is);
		shopPageAdmin.setItem(16, is);
		shopPageAdmin.setItem(17, is);

		shopPageAdmin.setItem(18, is);
		shopPageAdmin.setItem(22, is);
		shopPageAdmin.setItem(26, is);

		shopPageAdmin.setItem(27, is);
		shopPageAdmin.setItem(31, is);
		shopPageAdmin.setItem(35, is);

		shopPageAdmin.setItem(36, is);
		shopPageAdmin.setItem(40, is);
		shopPageAdmin.setItem(44, is);

		shopPageAdmin.setItem(45, is);
		shopPageAdmin.setItem(46, is);
		shopPageAdmin.setItem(47, is);
		shopPageAdmin.setItem(48, is);
		shopPageAdmin.setItem(49, is);
		shopPageAdmin.setItem(50, is);
		shopPageAdmin.setItem(51, is);
		shopPageAdmin.setItem(52, is);
		shopPageAdmin.setItem(53, is);

		/*
		 * Not gonna have multiple pages atm. is = new ItemStack(Material.NETHER_STAR);
		 * im = is.getItemMeta(); im.setDisplayName(Util.HideString("[shop.previous]") +
		 * ChatColor.RED + "Previous Page"); is.setItemMeta(im); shopPage.setItem(45,
		 * is);
		 * 
		 * is = new ItemStack(Material.NETHER_STAR); im = is.getItemMeta();
		 * im.setDisplayName(Util.HideString("[shop.next]") + ChatColor.YELLOW +
		 * "Next Page"); is.setItemMeta(im); shopPage.setItem(53, is);
		 */

	}

	public void createShopAdmin() {

		ItemStack is = new ItemStack(Material.CHEST);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(Util.HideString("[shop.inventory]") + ChatColor.YELLOW + "Shop Inventory");
		is.setItemMeta(im);
		this.stock = new ItemStack(is);

		shopPageAdmin.setItem(4, this.stock);

	}

	public void openShop(Player player) {
		User user = Users.getUser(player.getUniqueId().toString());
		for (int i = 0; i < this.sold.size(); i++) {
			// sold.get(i)
			ItemStack is = new ItemStack(sold.get(i));
			ItemMeta im = is.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add("Sell Item");
			im.setLore(lore);
			if (is != null) {
				if (shopPage.getItem(19) == null) {
					shopPage.setItem(19, is);
				} else if (shopPage.getItem(20) == null) {
					shopPage.setItem(20, is);
				} else if (shopPage.getItem(21) == null) {
					shopPage.setItem(21, is);

				} else if (shopPage.getItem(28) == null) {
					shopPage.setItem(28, is);
				} else if (shopPage.getItem(29) == null) {
					shopPage.setItem(29, is);
				} else if (shopPage.getItem(30) == null) {
					shopPage.setItem(30, is);

				} else if (shopPage.getItem(37) == null) {
					shopPage.setItem(37, is);
				} else if (shopPage.getItem(38) == null) {
					shopPage.setItem(38, is);
				} else if (shopPage.getItem(39) == null) {
					shopPage.setItem(39, is);
				}
			}
		}
		for (int i = 0; i < this.purchased.size(); i++) {
			// purchased.get(i)
			ItemStack is = new ItemStack(purchased.get(i));
			ItemMeta im = is.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add("Purchase Item");
			im.setLore(lore);
			if (is != null) {
				if (shopPage.getItem(23) == null) {
					shopPage.setItem(23, is);
				} else if (shopPage.getItem(24) == null) {
					shopPage.setItem(24, is);
				} else if (shopPage.getItem(25) == null) {
					shopPage.setItem(25, is);

				} else if (shopPage.getItem(32) == null) {
					shopPage.setItem(32, is);
				} else if (shopPage.getItem(33) == null) {
					shopPage.setItem(33, is);
				} else if (shopPage.getItem(34) == null) {
					shopPage.setItem(34, is);

				} else if (shopPage.getItem(41) == null) {
					shopPage.setItem(41, is);
				} else if (shopPage.getItem(42) == null) {
					shopPage.setItem(42, is);
				} else if (shopPage.getItem(43) == null) {
					shopPage.setItem(43, is);
				}
			}
		}

		if (owner.getName().equalsIgnoreCase(player.getName())) {
			player.openInventory(shopPageAdmin);
			user.setCurrentShop(shopId);
		} else {
			player.openInventory(shopPage);
			user.setCurrentShop(shopId);
		}
	}
}
