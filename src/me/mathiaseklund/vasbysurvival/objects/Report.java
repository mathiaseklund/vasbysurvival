package me.mathiaseklund.vasbysurvival.objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Reports;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Report {

	Main main = Main.getMain();

	String reportId;

	String reporterUUID;
	String reporterName;

	String targetUUID;
	String targetName;

	Block block;
	String reason;

	// CONSTRUCTORS

	public Report(Player player, Block block, String reason) {
		Long now = Calendar.getInstance().getTimeInMillis();
		this.reportId = "report" + now;
		Reports.addReport(reportId, this);
		this.reporterUUID = player.getUniqueId().toString();
		this.reporterName = player.getName();

		this.block = block;
		this.reason = reason;
		store();
		Reports.notifyStaff();
	}

	public Report(Player player, String targetUUID, String targetName, String reason) {
		Long now = Calendar.getInstance().getTimeInMillis();
		this.reportId = "report" + now;
		Reports.addReport(reportId, this);

		this.reporterUUID = player.getUniqueId().toString();
		this.reporterName = player.getName();

		this.targetUUID = targetUUID;
		this.targetName = targetName;
		this.reason = reason;
		store();
		Reports.notifyStaff();
	}

	public Report(String reportId, String reporterUUID, String reporterName, String blockLocation, String targetUUID,
			String targetName, String reason) {
		this.reportId = reportId;
		Reports.addReport(reportId, this);

		this.reporterUUID = reporterUUID;
		this.reporterName = reporterName;

		if (blockLocation != null) {
			this.block = Util.StringToLocation(blockLocation).getBlock();
		}

		this.targetUUID = targetUUID;
		this.targetName = targetName;
		this.reason = reason;
	}

	////////////////

	// STORAGE

	public void store() {
		File file = new File(main.getDataFolder() + File.separator + "reports" + File.separator,
				this.reportId + ".yml");
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileConfiguration f = new YamlConfiguration();
		try {
			f.load(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			f.set("reporter.uuid", this.reporterUUID);
			f.set("reporter.name", this.reporterName);
			f.set("reason", reason);
			if (this.block != null) {
				f.set("block", Util.LocationToString(this.block.getLocation()));
			}
			f.set("target.uuid", targetUUID);
			f.set("target.name", targetName);

			try {
				f.save(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				Util.debug("Stored report data");
			}
		}
	}

	///////////////

	public String getId() {
		return this.reportId;
	}

	public Block getBlock() {
		return this.block;
	}

	public String getReason() {
		return this.reason;
	}

	public String getTargetUUID() {
		return this.targetUUID;
	}

	public String getTargetName() {
		return this.targetName;
	}

	public String getReporterUUID() {
		return this.reporterUUID;
	}

	public String getReporterName() {
		return this.reporterName;
	}

}
