package me.mathiaseklund.vasbysurvival;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.vasbysurvival.api.Broadcaster;
import me.mathiaseklund.vasbysurvival.api.Reports;
import me.mathiaseklund.vasbysurvival.commands.console.CommandReload;
import me.mathiaseklund.vasbysurvival.commands.console.CommandRole;
import me.mathiaseklund.vasbysurvival.commands.premium.CommandColor;
import me.mathiaseklund.vasbysurvival.commands.premium.CommandCraft;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandBan;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandMute;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandReports;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandRollback;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandSetSpawn;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandStaffMode;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandUnban;
import me.mathiaseklund.vasbysurvival.commands.staff.CommandUnmute;
import me.mathiaseklund.vasbysurvival.commands.user.CommandChest;
import me.mathiaseklund.vasbysurvival.commands.user.CommandFriend;
import me.mathiaseklund.vasbysurvival.commands.user.CommandMOTD;
import me.mathiaseklund.vasbysurvival.commands.user.CommandMark;
import me.mathiaseklund.vasbysurvival.commands.user.CommandProtectBlocks;
import me.mathiaseklund.vasbysurvival.commands.user.CommandReport;
import me.mathiaseklund.vasbysurvival.commands.user.CommandRules;
import me.mathiaseklund.vasbysurvival.commands.user.CommandTrade;
import me.mathiaseklund.vasbysurvival.commands.user.VirtualChestListener;
import me.mathiaseklund.vasbysurvival.listeners.BlockListener;
import me.mathiaseklund.vasbysurvival.listeners.CommandListener;
import me.mathiaseklund.vasbysurvival.listeners.EntityListener;
import me.mathiaseklund.vasbysurvival.listeners.ExplosionListener;
import me.mathiaseklund.vasbysurvival.listeners.GraceListener;
import me.mathiaseklund.vasbysurvival.listeners.InteractListener;
import me.mathiaseklund.vasbysurvival.listeners.PlayerListener;
import me.mathiaseklund.vasbysurvival.listeners.ReportListener;
import me.mathiaseklund.vasbysurvival.listeners.ShopListener;
import me.mathiaseklund.vasbysurvival.listeners.SleepListener;
import me.mathiaseklund.vasbysurvival.listeners.StaffListener;
import me.mathiaseklund.vasbysurvival.listeners.TradeListener;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class Main extends JavaPlugin {

	static Main main;
	public static List<String> disabled = new ArrayList<String>();
	// Files
	File messagesFile;
	FileConfiguration messages;
	File dataFile;
	FileConfiguration data;

	/**
	 * Get instance of Main class
	 * 
	 * @return Main class
	 */
	public static Main getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();

		for (Player p : Bukkit.getOnlinePlayers()) {
			new User(p.getUniqueId().toString());
		}

		Broadcaster.startInfoLoop();
		Reports.loadStoredReports();

		loadProtectedBlocks();
	}

	/**
	 * Plugin is disabled
	 */
	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer("Server Restarting");
		}
	}

	/**
	 * Load default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		dataFile = new File(getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			saveResource("data.yml", false);
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
			List<String> list = data.getStringList("disabled");
			if (list != null) {
				disabled = list;
			} else {
				disabled = new ArrayList<String>();
			}
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save messages file
	 */
	public void saveMessages() {
		try {
			messages.save(messagesFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Save data file
	 */
	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Messages config
	 * 
	 * @return
	 */
	public FileConfiguration getMessages() {
		return this.messages;
	}

	/**
	 * Get Data config
	 * 
	 * @return
	 */
	public FileConfiguration getData() {
		return this.data;
	}

	public void loadProtectedBlocks() {
		File folder = new File(getDataFolder() + File.separator + "users" + File.separator);
		if (folder.exists()) {
			for (File file : folder.listFiles()) {
				FileConfiguration f = new YamlConfiguration();
				String name = file.getName().replaceAll(".yml", "");
				Util.debug("Loading protected blocks for user: " + name);
				try {
					f.load(file);
					List<String> blocks = f.getStringList("protected");
					if (blocks != null) {
						for (String s : blocks) {
							Location loc = Util.StringToLocation(s);
							loc.getBlock().setMetadata("owner", new FixedMetadataValue(this, name));
						}
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new GraceListener(), this);
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new ReportListener(), this);
		getServer().getPluginManager().registerEvents(new StaffListener(), this);
		getServer().getPluginManager().registerEvents(new InteractListener(), this);
		getServer().getPluginManager().registerEvents(new TradeListener(), this);
		getServer().getPluginManager().registerEvents(new EntityListener(), this);
		getServer().getPluginManager().registerEvents(new CommandListener(), this);
		getServer().getPluginManager().registerEvents(new ExplosionListener(), this);
		getServer().getPluginManager().registerEvents(new VirtualChestListener(), this);
		getServer().getPluginManager().registerEvents(new ShopListener(), this);
		getServer().getPluginManager().registerEvents(new SleepListener(), this);
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {
		getCommand("protectblocks").setExecutor(new CommandProtectBlocks());
		getCommand("staffmode").setExecutor(new CommandStaffMode());
		getCommand("mark").setExecutor(new CommandMark());
		getCommand("vsreload").setExecutor(new CommandReload());
		getCommand("report").setExecutor(new CommandReport());
		getCommand("reports").setExecutor(new CommandReports());
		getCommand("ban").setExecutor(new CommandBan());
		getCommand("unban").setExecutor(new CommandUnban());
		getCommand("color").setExecutor(new CommandColor());
		getCommand("mute").setExecutor(new CommandMute());
		getCommand("unmute").setExecutor(new CommandUnmute());
		getCommand("role").setExecutor(new CommandRole());
		getCommand("friend").setExecutor(new CommandFriend());
		getCommand("trade").setExecutor(new CommandTrade());
		getCommand("setspawn").setExecutor(new CommandSetSpawn());
		getCommand("chest").setExecutor(new CommandChest());
		getCommand("rollback").setExecutor(new CommandRollback());
		getCommand("craft").setExecutor(new CommandCraft());
		getCommand("rules").setExecutor(new CommandRules());
		getCommand("motd").setExecutor(new CommandMOTD());
		// getCommand("shop").setExecutor(new CommandShop());
	}
}
