package me.mathiaseklund.vasbysurvival.commands.user;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Trades;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandTrade implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 2) {
				if (args[0].equalsIgnoreCase("accept")) {
					// TODO accept a trade
					Util.debug("Accepting a trade.");
					Trades.acceptTrade(player.getUniqueId().toString(), args[1]);
				} else if (args[0].equalsIgnoreCase("decline")) {
					// TODO decline a trade
					Util.debug("Declining a trade.");
					Trades.declineTrade(player.getUniqueId().toString(), args[1]);
				}
			} else if (args.length == 1) {
				if (Users.getUser(player.getUniqueId().toString()).isPremium()) {
					String t = args[0];
					Player target = Bukkit.getPlayer(t);
					if (target != null) {
						if (!target.getName().equalsIgnoreCase(player.getName())) {
							Trades.requestTrade(player.getUniqueId().toString(), target.getUniqueId().toString());
						} else {
							Util.message(player, "&4ERROR:&7 You can't target yourself");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}
				} else {
					Util.message(player, "&4ERROR:&7 You need to be &6Premium&7 to use this command.");
				}
			} else {
				Util.message(player, "&7Usage:&e /trade <targetName>");
			}
		}
		return false;
	}

}
