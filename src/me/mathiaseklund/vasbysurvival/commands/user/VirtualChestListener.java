package me.mathiaseklund.vasbysurvival.commands.user;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class VirtualChestListener implements Listener {

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if (event.getPlayer() instanceof Player) {
			Player player = (Player) event.getPlayer();
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.getCurrentVC() != null) {
				String chestId = user.getCurrentVC();
				Util.debug("Closed Virtual Chest: " + chestId);
				user.storeVirtualChestItems(chestId, event.getView().getTopInventory());
				user.setCurrentVC(null);
			}
		}
	}
}
