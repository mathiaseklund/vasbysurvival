package me.mathiaseklund.vasbysurvival.commands.user;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandRules implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		for (String s : Main.getMain().getMessages().getStringList("rules")) {
			Util.message(sender, s);
		}
		return false;
	}

}
