package me.mathiaseklund.vasbysurvival.commands.user;

import java.util.Set;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandChest implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (args.length == 0) {
				// List all chests
				Set<String> chests = user.getVirtualChests();
				if (chests.isEmpty()) {
					Util.message(player, "&4ERROR:&7 You do not have any virtual chests.");
					Util.message(player, "&eYou can create a new Chest by doing &c/chest <chestId>&e!");
				} else {
					TextComponent t = new TextComponent("Virtual Chests: ");
					t.setColor(ChatColor.GRAY);
					TextComponent a = new TextComponent("[");
					a.setColor(ChatColor.GOLD);
					int am = 0;
					for (String s : chests) {
						Util.debug("Found chest: " + s);
						if (am < 1) {
							TextComponent c = new TextComponent(s);
							c.setColor(ChatColor.GOLD);
							c.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chest " + s));
							c.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									new ComponentBuilder("Click to Open!").create()));
							a.addExtra(c);
							am++;
						} else {
							TextComponent c = new TextComponent(", " + s);
							c.setColor(ChatColor.GOLD);
							c.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chest " + s));
							c.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									new ComponentBuilder("Click to Open!").create()));
							a.addExtra(c);
							am++;
						}
					}
					TextComponent b = new TextComponent("]");
					b.setColor(ChatColor.GOLD);
					a.addExtra(b);
					t.addExtra(a);

					player.spigot().sendMessage(t);
				}
			} else if (args.length > 0) {
				String chestId = args[0];
				chestId = chestId.replaceAll("\\.", "_");
				chestId = chestId.replaceAll("\"", "_");
				chestId = chestId.replaceAll("\\'", "_");
				if (user.hasVirtualChest(chestId)) {
					// Open chest
					Util.debug("Opening Chest: " + chestId);
					user.openVirtualChest(chestId);
				} else {
					// Create new chest
					Util.debug("Creating new chest. " + chestId);
					if (user.canCreateVirtualChest()) {
						user.createNewVirtualChest(chestId);
						user.openVirtualChest(chestId);
					} else {
						Util.message(player,
								"&4ERROR:&7 You already have a Virtual Chest. To create more virtual chests, please purchase &6Premium&7!");
					}
				}
			}
		}
		return false;
	}

}
