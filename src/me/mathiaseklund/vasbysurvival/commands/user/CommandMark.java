package me.mathiaseklund.vasbysurvival.commands.user;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandMark implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			HashMap<String, String> marks = user.getMarks();
			if (args.length == 0) {
				if (marks.isEmpty()) {
					Util.message(player, main.getMessages().getString("marks.nomarksfound"));
				} else {
					TextComponent t = new TextComponent("Marks: [");
					t.setColor(ChatColor.GRAY);
					int i = 0;
					for (String s : marks.keySet()) {
						TextComponent m;
						if (i == 0) {
							m = new TextComponent(s);
							m.setColor(ChatColor.GOLD);
							m.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									new ComponentBuilder(marks.get(s).replace("world ", "")).color(ChatColor.YELLOW)
											.create()));
							t.addExtra(m);
						} else {
							m = new TextComponent(", " + s);
							m.setColor(ChatColor.GOLD);
							m.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
									new ComponentBuilder(marks.get(s).replace("world ", "")).color(ChatColor.YELLOW)
											.create()));
							t.addExtra(m);
						}
						i++;

					}
					TextComponent t2 = new TextComponent("]");
					t2.setColor(ChatColor.GRAY);
					t.addExtra(t2);

					player.spigot().sendMessage(t);

				}
			} else if (args.length == 1) {
				String markId = args[0];
				markId = markId.replaceAll("\\.", "_");
				markId = markId.replaceAll("\\,", "_");
				markId = markId.replaceAll("\\'", "_");
				if (marks.containsKey(markId)) {
					if (user.isPremium()) {
						player.setCompassTarget(Util.StringToLocation(marks.get(markId)));
						Util.message(player,
								main.getMessages().getString("marks.tracking").replaceAll("%markId%", markId));
					} else {
						Util.message(player,
								main.getMessages().getString("marks.alreadyexists").replaceAll("%markId%", markId));
					}
				} else {
					user.addMark(markId);
					Util.message(player, main.getMessages().getString("marks.marked").replaceAll("%markId%", markId));
				}
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("remove")) {
					String markId = args[1];
					markId = markId.replaceAll("\\.", "_");
					markId = markId.replaceAll("\\,", "_");
					markId = markId.replaceAll("\\'", "_");
					if (marks.containsKey(markId)) {
						user.removeMark(markId);
						Util.message(player,
								main.getMessages().getString("marks.removed").replaceAll("%markId%", markId));
					}
				} else {
					Util.message(player, "&7Usage:&e /mark remove <id>");
				}
			}
		}
		return false;
	}

}
