package me.mathiaseklund.vasbysurvival.commands.user;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Friends;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandFriend implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (args.length == 2) { // /friend add/accept/decline <name>
				if (args[0].equalsIgnoreCase("add")) {
					Player t = Bukkit.getPlayer(args[1]);

					if (t != null) {
						if (!t.getName().equalsIgnoreCase(player.getName())) {
							Friends.addFriend(player.getUniqueId().toString(), t.getUniqueId().toString());
						} else {
							Util.message(player, "&4ERROR:&7 Can't target yourself.");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}

				} else if (args[0].equalsIgnoreCase("remove")) {
					Player t = Bukkit.getPlayer(args[1]);

					if (t != null) {
						if (!t.getName().equalsIgnoreCase(player.getName())) {
							Friends.removeFriend(player.getUniqueId().toString(), t.getUniqueId().toString());
						} else {
							Util.message(player, "&4ERROR:&7 Can't target yourself.");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}

				} else if (args[0].equalsIgnoreCase("accept")) {
					Player t = Bukkit.getPlayer(args[1]);
					if (t != null) {
						if (!t.getName().equalsIgnoreCase(player.getName())) {
							Friends.acceptRequest(player.getUniqueId().toString(), t.getUniqueId().toString());
						} else {
							Util.message(player, "&4ERROR:&7 Can't target yourself.");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}
				} else if (args[0].equalsIgnoreCase("decline")) {
					Player t = Bukkit.getPlayer(args[1]);
					if (t != null) {
						if (!t.getName().equalsIgnoreCase(player.getName())) {
							Friends.declineRequest(player.getUniqueId().toString(), t.getUniqueId().toString());
						} else {
							Util.message(player, "&4ERROR:&7 Can't target yourself.");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}
				} else {
					Util.message(player, "&7Usage:&e /friend add/accept/decline <playerName>");
				}
			} else if (args.length == 0) {
				List<String> friends = Users.getUser(player.getUniqueId().toString()).getFriends();
				String message = "&7Friends: &e[";
				for (String s : friends) {
					User user = Users.getUser(s);
					if (user != null) {
						if (message.equalsIgnoreCase("&7Friends: &e[")) {
							if (user.getPlayer() == null) {
								message = message + "&c" + user.getName() + "&e";
							} else {
								message = message + "&a" + user.getName() + "&e";
							}
						} else {
							if (user.getPlayer() == null) {
								message = message + ", &c" + user.getName() + "&e";
							} else {
								message = message + ", &a" + user.getName() + "&e";
							}
						}
					}
				}
				message = message + "]";
				Util.message(player, message);
			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("requests")) {
					String outgoing = "&7Outgoing: &e[";
					String incoming = "&7Incoming: &e[";

					for (String s : Users.getUser(player.getUniqueId().toString()).getOutgoingRequests()) {
						User user = Users.getUser(s);
						if (user != null) {
							if (outgoing.equalsIgnoreCase("&7Outgoing: &e[")) {
								if (user.getPlayer() == null) {
									outgoing = outgoing + "&c" + user.getName() + "&e";
								} else {
									outgoing = outgoing + "&a" + user.getName() + "&e";
								}
							} else {
								if (user.getPlayer() == null) {
									outgoing = outgoing + ", &c" + user.getName() + "&e";
								} else {
									outgoing = outgoing + ", &a" + user.getName() + "&e";
								}
							}
						}
					}
					outgoing = outgoing + "]";

					for (String s : Users.getUser(player.getUniqueId().toString()).getIncomingRequests()) {
						User user = Users.getUser(s);
						if (user != null) {
							if (incoming.equalsIgnoreCase("&7Incoming: &e[")) {
								if (user.getPlayer() == null) {
									incoming = incoming + "&c" + user.getName() + "&e";
								} else {
									incoming = incoming + "&a" + user.getName() + "&e";
								}
							} else {
								if (user.getPlayer() == null) {
									incoming = incoming + ", &c" + user.getName() + "&e";
								} else {
									incoming = incoming + ", &a" + user.getName() + "&e";
								}
							}
						}
					}
					incoming = incoming + "]";
					Util.message(player, outgoing);
					Util.message(player, incoming);
				}
			} else {
				Util.message(player, "&7Usage:&e /friend add/remove/requests/accept/decline [playerName]");
			}
		}
		return false;
	}

}
