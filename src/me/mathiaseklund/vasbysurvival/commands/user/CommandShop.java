package me.mathiaseklund.vasbysurvival.commands.user;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.Shop;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandShop implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (args.length == 2) {
				if (args[0].equalsIgnoreCase("create")) { // /shop create <shopId>
					String shopId = args[1].replaceAll("\\.", "");
					new Shop(user.getId(), shopId, true);
				} else {
					sendHelpMessage(player);
				}
			} else if (args.length == 4) {
				// TODO
			} else {
				sendHelpMessage(player);
			}
		}
		return false;
	}

	public void sendHelpMessage(Player player) {
		for (String s : Main.getMain().getMessages().getStringList("shop.help")) {
			Util.message(player, s);
		}
	}
}
