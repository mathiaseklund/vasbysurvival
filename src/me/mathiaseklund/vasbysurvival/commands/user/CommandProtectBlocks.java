package me.mathiaseklund.vasbysurvival.commands.user;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandProtectBlocks implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			user.toggleProtectBlocks();
			if (user.protectBlocks()) {
				Util.message(player, main.getMessages().getString("protectblocks.toggled.true"));
			} else {
				Util.message(player, main.getMessages().getString("protectblocks.toggled.false"));
			}
		}
		return false;
	}

}
