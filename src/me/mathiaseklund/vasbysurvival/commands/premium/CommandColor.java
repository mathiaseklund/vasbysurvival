package me.mathiaseklund.vasbysurvival.commands.premium;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandColor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isPremium()) {
				if (args.length == 2) {
					if (args[0].equalsIgnoreCase("name")) {
						String color = args[1];
						if (isColorCode(color)) {
							user.setNameColor(color);
							Util.message(player, "You've set your name color to '" + color + player.getName() + "&r'");
						} else {
							Util.message(player, "&4ERROR:&7 ColorCode must be a valid color code.");
						}
					} else if (args[0].equalsIgnoreCase("message")) {
						String color = args[1];
						if (isColorCode(color)) {
							user.setMessageColor(color);
							Util.message(player, "You've set your message color to '" + color + "message" + "&r'");
						} else {
							Util.message(player, "&4ERROR:&7 ColorCode must be a valid color code.");
						}
					} else {
						Util.message(player, "&7Usage:&e /color name/message <ColorCode (&e)>");
					}
				} else {
					Util.message(player, "&7Usage:&e /color name/message <ColorCode (&e)>");
				}
			}
		}
		return false;
	}

	public boolean isColorCode(String color) {
		if (color.equalsIgnoreCase("&0") || color.equalsIgnoreCase("&1") || color.equalsIgnoreCase("&2")
				|| color.equalsIgnoreCase("&3") || color.equalsIgnoreCase("&4") || color.equalsIgnoreCase("&5")
				|| color.equalsIgnoreCase("&8") || color.equalsIgnoreCase("&7") || color.equalsIgnoreCase("&6")
				|| color.equalsIgnoreCase("&9") || color.equalsIgnoreCase("&a") || color.equalsIgnoreCase("&b")
				|| color.equalsIgnoreCase("&e") || color.equalsIgnoreCase("&d") || color.equalsIgnoreCase("&c")
				|| color.equalsIgnoreCase("&f")) {
			return true;
		} else {
			return false;
		}
	}
}
