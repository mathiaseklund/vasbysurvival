package me.mathiaseklund.vasbysurvival.commands.staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandMute implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				if (args.length == 1) { // /mute <player>
					Player t = Bukkit.getPlayer(args[0]);
					if (t != null) {
						User target = Users.getUser(t.getUniqueId().toString());
						target.mute(0);
						Util.message(player, "&eYou've muted &c" + t.getName() + "&e!");
					} else {
						Util.message(player, "&4ERROR:&7 Target player not found.");
					}
				} else if (args.length > 1) { // /mute <player> <time>
					if (Util.isInteger(args[1])) {
						Player t = Bukkit.getPlayer(args[0]);
						if (t != null) {
							int time = Integer.parseInt(args[1]);
							User target = Users.getUser(t.getUniqueId().toString());
							target.mute(time);
							Util.message(player, "&eYou've muted &c" + t.getName() + "&e!");
						} else {
							Util.message(player, "&4ERROR:&7 Target player not found.");
						}
					} else {
						Util.message(player, "&4ERROR:&7 Mute time has to be a Integer.");
					}
				}
			}
		}
		return false;
	}

}
