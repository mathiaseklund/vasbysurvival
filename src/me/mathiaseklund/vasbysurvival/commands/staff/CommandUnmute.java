package me.mathiaseklund.vasbysurvival.commands.staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandUnmute implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				if (args.length > 0) { // /unmute <player>
					Player t = Bukkit.getPlayer(args[0]);
					User target = Users.getUser(t.getUniqueId().toString());
					target.unmute();
					Util.message(player, "&eYou've unmuted &c" + target.getName() + "&e!");
				} else {
					Util.message(player, "&7Usage:&e /unmute <player>");
				}
			}
		}
		return false;
	}

}
