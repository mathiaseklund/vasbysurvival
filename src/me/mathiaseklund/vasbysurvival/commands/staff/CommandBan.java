package me.mathiaseklund.vasbysurvival.commands.staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandBan implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				if (user.inStaffMode()) {
					if (args.length == 2) { // /ban <player> <reason>
						Player t = Bukkit.getPlayer(args[0]);
						String reason = args[1].replaceAll("_", " ");
						User target = Users.getUser(t.getUniqueId().toString());
						Util.message(player, "&cYou have banned " + target.getName() + ". Reason: " + reason);
						target.ban(reason, 0);
					} else if (args.length == 3) { // /ban <player> <reason> <time>
						if (Util.isInteger(args[2])) {
							Player t = Bukkit.getPlayer(args[0]);
							String reason = args[1].replaceAll("_", " ");
							int time = Integer.parseInt(args[2]);
							User target = Users.getUser(t.getUniqueId().toString());
							Util.message(player, "&cYou have banned " + target.getName() + " for " + time
									+ " Seconds. Reason: " + reason);
							target.ban(reason, time);
						} else {
							Util.message(player, "&4ERROR:&7 Ban time has to be an Integer.");
						}
					} else {
						Util.message(player, "&7Usage:&e /ban <player> <reason> [time]");
					}
				} else {
					Util.message(player, "&4ERROR:&7 You can only use this command in &cStaffmode&7.");
				}
			}
		} else {

		}
		return false;
	}

}
