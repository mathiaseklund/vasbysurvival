package me.mathiaseklund.vasbysurvival.commands.staff;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Logs;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandRollback implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				if (args.length == 1) {
					String name = args[0];
					Player t = Bukkit.getPlayer(name);
					if (t == null) {
						User target = Users.getUser(name);
						Logs.rollback(target.getId());
						Util.message(player, "&aRolledback all of " + target.getName() + "'s blocks this session");
					} else {
						Logs.rollback(t.getUniqueId().toString());
						Util.message(player, "&aRolledback all of " + t.getName() + "'s blocks this session");
					}

				} else {
					Util.message(player, "&7Usage:&e /rollback <targetName>/<targetUUID>");
				}
			}
		}
		return false;
	}

}
