package me.mathiaseklund.vasbysurvival.commands.staff;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandUnban implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				if (user.inStaffMode()) {
					if (args.length == 1) {
						String uuid = args[0];
						File file = new File(main.getDataFolder() + File.separator + "users" + File.separator,
								uuid + ".yml");
						if (file.exists()) {
							FileConfiguration f = new YamlConfiguration();
							try {
								f.load(file);
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvalidConfigurationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} finally {
								if (f.getBoolean("banned")) {
									f.set("banned", false);
									f.set("unbanon", null);
									f.set("banreason", null);
									try {
										f.save(file);
										Util.message(player, "&cYou have unbanned target player.");
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									Util.message(player, "&4ERROR:&7 Target player is not banned.");
								}
							}
						} else {
							Util.message(player, "&4ERROR:&7 Target UUID not found.");
						}
					} else {
						Util.message(player, "&7Usage:&e /unban <targetUUID>");
					}
				} else {
					Util.message(player, "&4ERROR:&7 You can only use this command in &cStaffmode&7.");
				}
			}
		}
		return false;
	}

}
