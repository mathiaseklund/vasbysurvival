package me.mathiaseklund.vasbysurvival.commands.staff;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;
import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandStaffMode implements CommandExecutor {
	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = Users.getUser(player.getUniqueId().toString());
			if (user.isStaff()) {
				user.toggleStaffMode();
				if (user.inStaffMode()) {
					Util.message(player, main.getMessages().getString("staffmode.toggled.true"));
				} else {
					Util.message(player, main.getMessages().getString("staffmode.toggled.false"));
				}
			}
		}
		return false;
	}

}
