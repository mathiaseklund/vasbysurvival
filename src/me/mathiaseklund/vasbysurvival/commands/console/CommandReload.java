package me.mathiaseklund.vasbysurvival.commands.console;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.Main;

public class CommandReload implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (!(sender instanceof Player)) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.kickPlayer("Server Restarting!");
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getMain(), new Runnable() {
				public void run() {
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "reload");
				}
			}, 20);
		}
		return false;
	}

}
