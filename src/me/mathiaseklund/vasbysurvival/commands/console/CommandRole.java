package me.mathiaseklund.vasbysurvival.commands.console;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vasbysurvival.api.Users;
import me.mathiaseklund.vasbysurvival.objects.User;
import me.mathiaseklund.vasbysurvival.util.Util;

public class CommandRole implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (!(sender instanceof Player)) { // /role add/remove <player> <role>
			if (args.length == 3) {
				if (args[0].equalsIgnoreCase("add")) {
					Player t = Bukkit.getPlayer(args[1]);
					if (t != null) {
						String role = args[2].toUpperCase();
						if (role.equalsIgnoreCase("PREMIUM") || role.equalsIgnoreCase("STAFF")) {
							User user = Users.getUser(t.getUniqueId().toString());
							user.addRole(role);
							Util.message(sender, "&eAdded &c" + role + "&e Role to " + user.getName());
						} else {
							Util.message(sender, "&4ERROR:&7 Selected Role does not exist.");
						}
					} else {
						Util.message(sender, "&4ERROR:&7 Target player not found.");
					}
				} else if (args[0].equalsIgnoreCase("remove")) {
					Player t = Bukkit.getPlayer(args[1]);
					if (t != null) {
						String role = args[2].toUpperCase();
						if (role.equalsIgnoreCase("PREMIUM") || role.equalsIgnoreCase("STAFF")) {
							User user = Users.getUser(t.getUniqueId().toString());
							user.removeRole(role);
							Util.message(sender, "&eRemoved &c" + role + "&e Role from " + user.getName());
						} else {
							Util.message(sender, "&4ERROR:&7 Selected Role does not exist.");
						}
					} else {
						Util.message(sender, "&4ERROR:&7 Target player not found.");
					}
				}
			} else {
				Util.message(sender, "&7Usage:&e /role add/remove <player> <role>");
			}
		}
		return false;
	}

}
